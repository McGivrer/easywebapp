# E@syWebApp

E@sywebapp is a sample application to demonstrate how to rules some framework all together on the 
JavaEE layers to perform an easy to develop and maintain java application.

current version : `0.0.5-SNAPSHOT`

## Goals

First, here will be build a web app based on Standard Servlet API, JPA with hibernate, well known 
Log4J and some exotic technology like Groovy for page rendering.
The embedded components are:

- Log4J
- Hibernate
- Groovy
- Servlet API (3.0 here but no impact about api version can run on 2.4 and 2.5 too)

Development is agile oriented through the embedded Jetty server with the maven build system. 
Using Eclipse upon all these fantastic tools and API is an easy way to the faaaast developping process. 

## Git Repository

A public repository is open on bitbicket.org:

[https://bitbucket.org/McGivrer/easywebapp](https://bitbucket.org/McGivrer/easywebapp) give access to the project page.

Cloning the repository with this simppe command line:

{{{
	git clone https://bitbucket.org/McGivrer/easywebapp.git
}}}

## Run this demo

You can easily run this demo on the embedded jetty server: just follow instruction on the page :
 [Maven + Jetty in eclipse to debug](http://mcgivrer.fr/mavenjetty-in-eclipse-to-debug) .


# TODO / Roadmap :
- EhCache started:
-- create pom references 
-- ehcache.xml basicaly created.
- ORM done : 
-- integrate Hibernate 
-- basic Entity Manager (add Entity detection through the Reflections library.
- AppController feature
-- implement the basic of Dispatcher and url recognition with Route system.
-- add BeforeControl and AfterControl annotations to perform 
-- need to add AppController method's parameters binding  
- Groovy started
-- need to implement more integration feature like list, pagination, form
-- basic rendergin is ok.
- Markdown support for static pages
-- a new idea would be to integrate the Markdown support to display some data field (for some CMS features). Maybe a simple helper in the groovy template to transform simple markdowned text from entity to HTML format.

# References
 - [http://logging.apache.org/log4j/1.2/index.html](http://logging.apache.org/log4j/1.2/index.html)
 - [http://docs.jboss.org/hibernate/orm/4.1/devguide/en-US/html_single/](http://docs.jboss.org/hibernate/orm/4.1/devguide/en-US/html_single/)
 - [http://groovy.codehaus.org/Groovy+Templates](http://groovy.codehaus.org/Groovy+Templates)
 - [http://jcp.org/aboutJava/communityprocess/final/jsr154/index.html](http://jcp.org/aboutJava/communityprocess/final/jsr154/index.html)

to be continued ...

McG.
