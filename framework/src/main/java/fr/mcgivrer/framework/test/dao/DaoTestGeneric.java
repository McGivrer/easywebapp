package fr.mcgivrer.framework.test.dao;

import fr.mcgivrer.framework.web.engine.configuration.Configuration;
import fr.mcgivrer.framework.web.engine.configuration.ConfigurationException;

public class DaoTestGeneric {

	protected Configuration config = null;

	protected void loadConfig() {
		// Read the Unit test Configuration file.
		try {
			config = Configuration.getInstance().read(
					this.getClass().getResource("/").getPath()
							+ "configuration/application.ini");
		} catch (ConfigurationException e) {
			System.out.println("FATAL Error during load configuration");
		}
	}

}
