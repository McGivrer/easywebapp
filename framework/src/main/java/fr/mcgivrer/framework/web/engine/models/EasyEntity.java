/**
 * 
 */
package fr.mcgivrer.framework.web.engine.models;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * @author mcgivrer
 * 
 */
@MappedSuperclass
public class EasyEntity implements Serializable, EEntity {
	/**
	 * 
	 */
	protected static final long serialVersionUID = -2697466834950889737L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Object getField(String field) {
		return null;
	}

	@Override
	public void setField(String field, Object value) {
	}

}
