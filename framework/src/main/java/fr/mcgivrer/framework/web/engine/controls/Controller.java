/**
 * 
 */
package fr.mcgivrer.framework.web.engine.controls;

import java.util.Map;

import fr.mcgivrer.framework.web.engine.services.render.exceptions.PageRenderException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Default signature for an application controller.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public interface Controller {

	/**
	 * Bind an object to the page.
	 * 
	 * @param key
	 * @param value
	 */
	public void bind(String key, Object value);

	/**
	 * Bind all the contained map values.
	 * 
	 * @param values
	 */
	public void bindMap(Map<String, String> values);

	/**
	 * Rendering of the page.
	 * 
	 * @param template
	 * @throws PageRenderException
	 */

	public String render(String template) throws PageRenderException;

	/**
	 * Rendering of the page.
	 * 
	 * @param template
	 * @throws PageRenderException
	 */
	public String render(String template, Map<String, Object> pBindings)
			throws PageRenderException;

    /**
     * Set the Http Request/response from Servlet.
     * @param req
     * @param resp
     */
    public void setRequest(HttpServletRequest req, HttpServletResponse resp);
}
