/**
 * 
 */
package fr.mcgivrer.framework.web.engine.html.elements.input;

import fr.mcgivrer.framework.web.engine.html.HtmlPlugin;

/**
 * Generate a Text Html form element.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class TextHtmlPlugin extends InputHtmlPlugin implements HtmlPlugin {
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.html.elements.input.InputHtmlPlugin#
	 * getType()
	 */
	@Override
	public String getType() {
		return "text";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.html.elements.FormElementHtmlPlugin#
	 * getTemplateElement()
	 */
	public String getTemplateElement() {
		return "@@label<br /><input @@attributes/>";
	}
}
