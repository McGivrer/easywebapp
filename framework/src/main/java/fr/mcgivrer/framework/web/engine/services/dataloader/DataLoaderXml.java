/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.dataloader;

import java.util.ResourceBundle;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;

/**
 * <p>
 * DataLoadXml is service to load XML formated data to database.
 * </p>
 * <p>
 * Create a new Connection to database with configuration from file
 * <code>configuration/db.properties</code>.
 * </p>
 * 
 * @author mcgivrer
 * 
 */
public class DataLoaderXml {

	/**
	 * Database connection information.
	 */
	private ResourceBundle dbConfig = ResourceBundle.getBundle(this.getClass()
			.getResource("/").getPath()
			+ "configuration/db.properties");

	/**
	 * Load the xmlDataFile and load data to configurated database.
	 * 
	 * @param xmlDataFile
	 *            the filename (fullpath) to the xml file to provide data to
	 *            set.
	 * @throws Exception
	 */
	public void loadData(String xmlDataFile) throws Exception {

		// Connect to database.
		IDatabaseTester databaseTester = new JdbcDatabaseTester(
				dbConfig.getString("db.jdbcdriver"),
				dbConfig.getString("db.url"), dbConfig.getString("username"),
				dbConfig.getString("db.password"), dbConfig.getString("schema"));
		// load xml file
		IDataSet dataSet = new FlatXmlDataSetBuilder().build(this.getClass()
				.getResourceAsStream(xmlDataFile));
		databaseTester.setDataSet(dataSet);
		// insert data.
		databaseTester.onSetup();
	}

	/**
	 * <p>
	 * Export data to XML file.
	 * </p>
	 * <p>
	 * TODO implement this Export Xml data from
	 * </p>
	 * database.
	 * 
	 * @param xmlDataFile
	 *            name of the file (fullpath) to be generated.
	 */
	public void exportData(String xmlDataFile) {

	}
}
