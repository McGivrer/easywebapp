/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.render;

import java.util.Map;

import fr.mcgivrer.framework.web.engine.services.render.exceptions.PageRenderException;

/**
 * @author frederic
 * 
 */
public interface PageRender {

	/**
	 * Render current action page.
	 * 
	 * @return
	 * @throws PageRenderException
	 */
	String render() throws PageRenderException;

	/**
	 * Render action with the following <code>template</code>.
	 * 
	 * @param template
	 * @return
	 * @throws PageRenderException
	 */
	String render(String template) throws PageRenderException;

	/**
	 * Render page with <code>template</code> according to <code>bindings</code>
	 * values.
	 * 
	 * @param binds
	 * @param template
	 * @throws PageRenderException
	 * @return
	 */
	String render(Map<String, Object> binds, String template)
			throws PageRenderException;

	/**
	 * bind <code>value</code> object to the <code>key</code> for render
	 * process.
	 * 
	 * @param key
	 * @param value
	 */
	void bind(String key, Object value);

	/**
	 * remove bound object corresponding to this <code>key</code>.
	 * 
	 * @param key
	 */
	void remove(String key);

	/**
	 * Flush rendering page.
	 */
	 void flushBindings();

	/**
	 * get all objects bind to template.
	 */
	 Map<String, Object> getBindings();

	/**
	 * Prepare the render template name generation with transmitting the action
	 * to perform.
	 * 
	 * @param action
	 */
	 void setAction(String action);

	/**
	 * Generate a standard error page without Render implementation. Only Java.
	 * 
	 * @param title
	 * @param content
	 * @return
	 */
	 String getErrorPage(String title, String content);

	/**
	 * Set the path to the templates theme.
	 * 
	 * @param relativePathTheme
	 */
	 void setTheme(String relativePathTheme);

	/**
	 * return the path to the current Templates path theme.
	 * 
	 * @return
	 */
	 String getTheme();

}