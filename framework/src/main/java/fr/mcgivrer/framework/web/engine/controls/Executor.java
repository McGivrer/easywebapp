/**
 * 
 */
package fr.mcgivrer.framework.web.engine.controls;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import fr.mcgivrer.framework.web.engine.configuration.modules.routes.Route;
import fr.mcgivrer.framework.web.engine.configuration.modules.routes.RoutesGroup;
import fr.mcgivrer.framework.web.engine.controls.annotations.AfterControl;
import fr.mcgivrer.framework.web.engine.controls.annotations.BeforeControl;
import fr.mcgivrer.framework.web.engine.error.ErrorPage;
import fr.mcgivrer.framework.web.engine.html.form.FormOutput;
import fr.mcgivrer.framework.web.engine.services.i18n.Messages;
import fr.mcgivrer.framework.web.engine.services.render.exceptions.PageRenderException;

/**
 * Controller manager. Execute annotated controller.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class Executor {
	/**
	 * default logger for this component.
	 */
	private static Logger logger = Logger.getLogger(Executor.class);

	/**
	 * Execute the corresponding <code>action</code> (method) from
	 * <code>controller</code> and generate corresponding <code>page</code>.
	 * 
	 * @param req
	 * @param resp
	 * @param route
	 * @param controller
	 * @param action
	 * @param page
	 * @return
	 */
	public static String call(HttpServletRequest req, HttpServletResponse resp,
			Route route, Controller controller, Method action, String page) {
		try {
			// Call BeforeControl annotated methods
			executeAnnotatedMethods(controller, BeforeControl.class);

			// execute Controller action.
			String templateController = (String) action.invoke(controller,
					(Object[]) null);

			// Add i18N component to Render context.
			controller.bind("messages", Messages.getInstance());
			controller.bind("form", FormOutput.getInstance());

			// render page for action controller
			String pageController = controller.render(templateController);

			// Call AfterControl annotated methods.
			executeAnnotatedMethods(controller, AfterControl.class);

			// Add template rendered to the page.
			StringBuffer sb = new StringBuffer(page);
			sb.append(pageController);
			page = sb.toString();

		} catch (IllegalArgumentException e) {
			ErrorPage.display(req, resp, "Error during Controller action call",
					e);
			logger.error("Error during Controller action call", e);
		} catch (IllegalAccessException e) {
			ErrorPage.display(req, resp, "Error during Controller action call",
					e);
			logger.error("Error during Controller action call", e);
		} catch (InvocationTargetException e) {
			ErrorPage.display(req, resp, "Error during Controller action call",
					e);
			logger.error("Error during Controller action call", e);
		} catch (PageRenderException e) {
			ErrorPage.display(req, resp, "Error during Page rendering", e);
			logger.error("Error during Page rendering", e);
		}
		return page;
	}

	/**
	 * Execute methods annotated with the <code>@BefioreControl</code> and
	 * <code>@AfterControl</code> from the
	 * <code>fr.mcgivrer.framework.web.application.controllers</code> package.
	 * 
	 * @param controller
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	private static void executeAnnotatedMethods(Controller controller,
			Class<? extends Annotation> annotationClazz)
			throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		// Call all the BeforeControl and AfterControl annotated method from the
		// controller class.
		ConfigurationBuilder config = new ConfigurationBuilder()
				.addUrls(
						ClasspathHelper
								.forPackage(RoutesGroup.FRAMEWORK_PCK_BASE_CONTROLLER))
				.addUrls(ClasspathHelper.forClass(BeforeControl.class),
						ClasspathHelper.forClass(AfterControl.class))
				.setScanners(new ResourcesScanner(),
						new TypeAnnotationsScanner(), new SubTypesScanner(),
						new MethodAnnotationsScanner());

		Reflections reflexMethods = new Reflections(config);
		// parse found methods
		Set<Method> beforeControls = reflexMethods
				.getMethodsAnnotatedWith(annotationClazz);
		if (beforeControls.size() > 0) {
			for (Method beforeControl : beforeControls) {
				if (beforeControl.getDeclaringClass().equals(
						controller.getClass())) {
					beforeControl.invoke(controller, (Object[]) null);
				}
			}
		}
	}
}
