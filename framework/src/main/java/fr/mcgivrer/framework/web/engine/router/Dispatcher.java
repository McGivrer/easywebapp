/**
 * 
 */
package fr.mcgivrer.framework.web.engine.router;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.Locale;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import fr.mcgivrer.framework.web.engine.configuration.Configuration;
import fr.mcgivrer.framework.web.engine.configuration.modules.routes.Route;
import fr.mcgivrer.framework.web.engine.controls.Controller;
import fr.mcgivrer.framework.web.engine.controls.Executor;
import fr.mcgivrer.framework.web.engine.debug.DebugMessage;
import fr.mcgivrer.framework.web.engine.error.ErrorPage;
import fr.mcgivrer.framework.web.engine.services.i18n.Messages;
import fr.mcgivrer.framework.web.engine.services.persistence.PersistenceService;

/**
 * <p>
 * This is the Request Dispatcher according to URL pattern, and declared
 * controllers.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class Dispatcher extends HttpServlet {
	/**
	 * default logger for this component.
	 */
	private static Logger logger = Logger.getLogger(Dispatcher.class);

	@SuppressWarnings("unused")
	private static RouterConfiguration configuration = null;

	/**
	 * Serializer identifier.
	 */
	private static final long serialVersionUID = 2623158536304612355L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		/**
		 * Extract configuration file path from servlet init params and context
		 * relative path.
		 */
		String configurationFilePath = config
				.getInitParameter("configuration-file");

		if (configurationFilePath != null && !configurationFilePath.equals("")) {
			// compose full path from relative one.
			configurationFilePath = getServletContext().getRealPath(
					configurationFilePath);
			// read configuration file (delegate to Configuration.
			configuration = new RouterConfiguration(configurationFilePath);
		} else {
			throw new ServletException(
					"Can't read the configuration file: configuration-file for Disptacher servlet is not set.");
		}
	}

	/**
	 * Initialize ORM layer
	 */
	private void initializeORM() {
		PersistenceService.getInstance();
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		logger.debug("dispatch delete: " + req.getPathInfo());
		dispatch(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		logger.debug("dispatch post: " + req.getPathInfo());
		dispatch(req, resp);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		logger.debug("dispatch put: " + req.getPathInfo());

		dispatch(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		logger.debug("dispatch get: " + req.getPathInfo());

		dispatch(req, resp);
	}

	/**
	 * Call the right controller based on the request <code>req</code> content.
	 * 
	 * @param req
	 * @param resp
	 * @throws ServletException
	 */
	protected void dispatch(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException {

		String pathInfo = req.getPathInfo();
		if (!isResourceRequest(req)) {
			if (Configuration.get("routes", req.getPathInfo()) != null) {
				Route route = (Route) Configuration.get("routes", pathInfo);
				logger.debug(String.format("retrieve Controller %s",
						route.toString()));

				// Set locale for messages.
				Locale locale = req.getLocale();
				Messages.setLocale(locale);

				try {
					Controller controller = route.getController().newInstance();

					// set the request and response into controller
					controller.setRequest(req, resp);

					Method action = route.getMethod();

					String page = "";
					// Execute controller method according to the url.
					page = Executor.call(req, resp, route, controller, action,
							page);

					Writer writer = resp.getWriter();
					writer.write(page);
				} catch (IOException ioe) {
					logger.error("Error during Page writing to HTTP Response",
							ioe);
					ioe.printStackTrace();

				} catch (InstantiationException ie) {
					logger.error("unable to instanciate the controller");
					ie.printStackTrace();

				} catch (IllegalAccessException iae) {
					logger.error("unable to access to the controller");
					iae.printStackTrace();
				}

				// display some debug informations.
				if (Configuration.isOn("debug", "showController")) {
					DebugMessage.show(resp, route.toString());
				}
			} else {
				ErrorPage.display(
						req,
						resp,
						String.format("Page for url %s is unknown",
								req.getPathInfo()), null);
			}
		} else {
			streamResource(req, resp);
		}
	}

	/**
	 * Stream resource to request
	 * 
	 * @param req
	 * @param resp
	 */
	private void streamResource(HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub
		String res = req.getPathInfo();
		String fullPathRes=this.getServletContext().getRealPath(res);
		logger.info("stream resource as '"+fullPathRes+"'");
		resp.addHeader("resource", fullPathRes);
		
	}

	/**
	 * Is request concern a Web resources like CSS, Javascript or media ?
	 * 
	 * @param req
	 * @return
	 */
	private boolean isResourceRequest(HttpServletRequest req) {
		return (req.getPathInfo().contains("/css")
				|| req.getPathInfo().contains("/js") || req.getPathInfo()
				.contains("/media"));
	}

}
