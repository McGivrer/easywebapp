/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.render.groovy;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.groovy.control.CompilationFailedException;

import fr.mcgivrer.framework.web.engine.services.EasyService;
import fr.mcgivrer.framework.web.engine.services.render.GenericPageRender;
import fr.mcgivrer.framework.web.engine.services.render.PageRender;
import fr.mcgivrer.framework.web.engine.services.render.exceptions.PageRenderException;
import groovy.text.SimpleTemplateEngine;
import groovy.text.Template;

/**
 * <p>
 * Groovy Page Template Rendering service.
 * </p>
 * <p>
 * Use the SimpleTemplateEngine implementation from Groovy to provide an easy to
 * use Page Template system.
 * </p>
 * 
 * @author frederic
 * 
 */
@EasyService(name = "GroovyPageRenderService", startlevel = 2)
public class GroovyPageRender extends GenericPageRender implements PageRender {

	private static SimpleTemplateEngine ste = null;

	private static Logger log = Logger.getLogger(GroovyPageRender.class);

	public GroovyPageRender() {
		init();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.easyweb.framework.render.PageRender#render()
	 */
	public String render() throws PageRenderException {
		return render(bindings, action + defaultTemplateExtension);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.easyweb.framework.render.PageRender#render(java.lang.String)
	 */
	public String render(String template) throws PageRenderException {
		return render(bindings, template);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.easyweb.framework.render.PageRender#render(java.util.Map,
	 * java.lang.String)
	 */
	public String render(Map<String, Object> binds, String template)
			throws PageRenderException {
		Template tpl = null;
		String templateFilePath = null;
		String message = "";
		String page = null;
		try {
			if (template == null) {
				template = (action == null || action.equals("") ? "index"
						: action);
			}
			template = (template.contains(".") ? template : template
					+ defaultTemplateExtension);

			templateFilePath = this.getClass().getResource(baseTemplatePath)
					.getPath()
					+ File.separator + template;
			tpl = ste.createTemplate(new File(templateFilePath));
			log.info("template [" + templateFilePath + "] loaded.");
			page = tpl.make(binds).toString();
		} catch (CompilationFailedException e) {
			message = "Rendering Error: can't compile template : " + template;
			page = getErrorPage("PageRender Error", message);
			log.error(message);
			throw new PageRenderException(message, e);
		} catch (ClassNotFoundException e) {
			message = "Rendering Error: error while compiling template unknown class in template: "
					+ template;
			page = getErrorPage("PageRender Error", message);
			log.error(message);
			throw new PageRenderException(message, e);
		} catch (IOException e) {
			message = "Rendering Error: template not found : " + template;
			page = getErrorPage("PageRender Error", message);
			log.error(message);
			throw new PageRenderException(message, e);
		}
		return page;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.easyweb.framework.render.PageRender#flush()
	 */
	public void flushBindings() {

	}

	@Override
	public void init() {
		if (ste == null) {
			ste = new SimpleTemplateEngine();
			log.info("GroovyPageRender:SimpleTemplateEngine instantiated");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.services.Service#getName()
	 */
	@Override
	public String getName() {

		return "GroovyRenderService";
	}

}
