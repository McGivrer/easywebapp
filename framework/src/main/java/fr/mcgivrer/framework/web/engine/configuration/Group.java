/**
 * 
 */
package fr.mcgivrer.framework.web.engine.configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * @author mcgivrer
 * 
 */
public interface Group<T> {
	/**
	 * return the group name the group module is able to read.
	 * 
	 * @return
	 */
	public String getGroupName();

	/**
	 * read line of configuration and convert it into Parameter.
	 * 
	 * @param line
	 */
	public void readLine(String line);

	/**
	 * Read the buffer to retrieve configuration data.
	 * 
	 * @param bufRead
	 * @return
	 */
	public BufferedReader read(BufferedReader bufRead) throws IOException;

	/**
	 * to be executed just before file processing.
	 */
	public void before();

	/**
	 * Retrieve the item from configuration list.
	 * 
	 * @param key
	 *            key of the needed item.
	 * @return T instance of the corresponding value.
	 */
	public T get(String key);

	/**
	 * Retirn all item from parameter's group.
	 * 
	 * @return
	 */
	public Map<String, T> getAll();

	public Properties getAllProps();
	
	public Object convertToProperty(T t);

}