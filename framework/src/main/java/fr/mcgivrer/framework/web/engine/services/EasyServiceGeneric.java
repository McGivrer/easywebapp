/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services;

import javax.security.auth.login.Configuration;

import org.apache.log4j.Logger;

/**
 * Generic implementation for service.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class EasyServiceGeneric implements Service {

	protected Configuration configuration = Configuration.getConfiguration();

	/**
	 * Internal logger.
	 */
	private static final Logger logger = Logger
			.getLogger(EasyServiceGeneric.class);

	@Override
	public void init() throws Exception {

	}

	@Override
	public void stop() throws Exception {
		logger.info("Stop service " + this.getName());

	}

	@Override
	public String getName() {
		return "undefined";
	}

}
