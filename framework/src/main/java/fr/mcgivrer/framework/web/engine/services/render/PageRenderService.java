/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.render;

import org.apache.log4j.Logger;

import fr.mcgivrer.framework.web.engine.configuration.Configuration;
import fr.mcgivrer.framework.web.engine.configuration.Parameter;
import fr.mcgivrer.framework.web.engine.services.EasyService;
import fr.mcgivrer.framework.web.engine.services.EasyServiceGeneric;
import fr.mcgivrer.framework.web.engine.services.Service;

/**
 * <p>
 * PageRenderService
 * </p>
 * <p>
 * Define the template engine use to render pages.
 * </p>
 * <p>
 * the default implementation propose the Groovy SimpleTemplateEngine.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
@EasyService(name="PageRenderService",startlevel = 30)
public class PageRenderService extends EasyServiceGeneric implements Service {

	private static final Logger logger = Logger
			.getLogger(PageRenderService.class);
	/**
	 * Default page renderer implementation.
	 */
	protected static PageRender renderInstance;

	protected static PageRenderService instance;

	/**
	 * internal constructor.
	 */
	public PageRenderService() {
		try {
			init();
		} catch (Exception e) {
			logger.fatal("Page Render Service not correctly initialized");
		}
	}

	/**
	 * return the instance for the configured PageRender into configuration
	 * file.
	 * 
	 * @return
	 */
	public PageRender getRender() {
		return renderInstance;
	}

	/**
	 * retrieve the sngleton instance for the PageRenderService.
	 * 
	 * @return
	 */
	public static PageRenderService getInstance() {
		if (instance == null) {
			instance = new PageRenderService();
		}
		return instance;
	}

	@Override
	public void init() throws Exception {
		Configuration.getInstance();
		String renderEngineImplName = ((Parameter) Configuration.get("render",
				"engine")).getValue();
		try {
			Class<?> renderEngine = Class.forName(renderEngineImplName);
			if (renderInstance == null) {
				renderInstance = (PageRender) renderEngine.newInstance();
			}
		} catch (ClassNotFoundException e) {
			logger.fatal(String.format("Page Render '%s' not found",
					renderEngineImplName));
		} catch (InstantiationException e) {
			logger.fatal(String.format(
					"Unable to instantiate the new Page Render '%s'",
					renderEngineImplName));
		} catch (IllegalAccessException e) {
			logger.fatal(String.format("Page Render '%s' not accessible",
					renderEngineImplName));
		}
	}

	@Override
	public String getName() {
		return "render";
	}
}
