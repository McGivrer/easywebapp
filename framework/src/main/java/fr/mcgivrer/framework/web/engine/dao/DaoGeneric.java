package fr.mcgivrer.framework.web.engine.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

import fr.mcgivrer.framework.web.engine.configuration.Configuration;
import fr.mcgivrer.framework.web.engine.services.persistence.PersistenceService;
import fr.mcgivrer.framework.web.engine.services.persistence.exception.ORMException;

/**
 * <p>
 * GenericDAO providing base of all database operation on entity T.
 * </p>
 * 
 * <p>
 * Largely inspired by the following GIST: https://gist.github.com/1261256
 * Thanks to augusto for providing such a cool implementation.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 * @param <T>
 * @param <PK>
 */
public class DaoGeneric<T extends Serializable, PK extends Serializable>
		implements Dao<T, PK> {

	private static final Logger logger = Logger.getLogger(DaoGeneric.class);

	/**
	 * Entity class manager by the DaoGeneric.
	 */
	protected Class<T> entityClass;

	/**
	 * Entity manager.
	 */
	protected EntityManager entityManager = null;

	/**
	 * loaded persistence unit.
	 */
	protected String persistenceUnit = "application";

	/**
	 * <p>
	 * DAO constructor on the default <code>persistenceUnit</code>
	 * ("application").
	 * </p>
	 * <p>
	 * Initialize <code>entityManagerFactory</code> and
	 * <code>entityManager</code> based on the selected
	 * <code>persistenceUnit</code>.
	 */
	public DaoGeneric() {
		retrieveEntityClass();
		intializePersistentUnit(persistenceUnit);
	}

	/**
	 * Retrieve the Entity <code>T</code> to be managed by this
	 * <code>DaoGeneric<T></code> implementation
	 */
	protected void retrieveEntityClass() {
		ParameterizedType genericSuperClass = (ParameterizedType) getClass()
				.getGenericSuperclass();
		@SuppressWarnings("unchecked")
		Class<T> class1 = (Class<T>) genericSuperClass.getActualTypeArguments()[0];
		this.entityClass = class1;
	}

	/**
	 * /**
	 * <p>
	 * Parameterized DAO constructor.
	 * </p>
	 * <p>
	 * Initialize <code>entityManagerFactory</code> and
	 * <code>entityManager</code> based on the selected
	 * <code>persistenceUnit</code>.
	 * 
	 * @param persistenceUnit
	 */
	public DaoGeneric(String persistenceUnit) {
		ParameterizedType genericSuperClass = (ParameterizedType) getClass()
				.getGenericSuperclass();
		@SuppressWarnings("unchecked")
		Class<T> class1 = (Class<T>) genericSuperClass.getActualTypeArguments()[0];
		this.entityClass = class1;
		intializePersistentUnit(persistenceUnit);
	}

	/**
	 * Initialize Entity Manager Factory with the <code>peristentUnit</code>.
	 */
	private void intializePersistentUnit(String persistenceUnit) {
		try {
			Configuration.getInstance();
			entityManager = PersistenceService.getInstance().getEntityManager(
					persistenceUnit);
		} catch (ORMException e) {
			logger.error("Can not get the entityManager for the"
					+ this.getClass().getSimpleName() + " DAO implemenattion.",
					e);
		}
	}

	@Override
	public T save(T entity) {
		boolean active = false;
		if (!(active = entityManager.getTransaction().isActive()) == true) {
			entityManager.getTransaction().begin();
		}
		entityManager.persist(entity);
		if (!active) {
			entityManager.getTransaction().commit();
		}
		return entity;
	}

	@Override
	public T merge(T entity) {
		boolean active = false;
		if (!(active = entityManager.getTransaction().isActive()) == true) {
			entityManager.getTransaction().begin();
		}
		entity = entityManager.merge(entity);
		if (!active) {
			entityManager.getTransaction().commit();
		}
		return entity;
	}

	@Override
	public void delete(T entity) {
		boolean active = false;
		if (!(active = entityManager.getTransaction().isActive()) == true) {
			entityManager.getTransaction().begin();
		}
		entityManager.remove(entity);
		if (!active) {
			entityManager.getTransaction().commit();
		}
	}

	@Override
	public T findById(PK id) {
		return this.entityManager.find(entityClass, id);
	}

	@Override
	public List<T> findAll() {
		return findByCriteria();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByNamedQuery(final String name, Object... params) {
		javax.persistence.Query query = entityManager.createNamedQuery(name);

		for (int i = 0; i < params.length; i++) {
			query.setParameter(i + 1, params[i]);
		}

		return (List<T>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByNamedQueryAndNamedParams(final String name,
			final Map<String, ?> params) {
		javax.persistence.Query query = entityManager.createNamedQuery(name);

		for (final Map.Entry<String, ?> param : params.entrySet()) {
			query.setParameter(param.getKey(), param.getValue());
		}

		return (List<T>) query.getResultList();
	}

	@Override
	public int countAll() {
		return getCount();
	}

	protected List<T> findByCriteria(final Criterion... criterion) {
		return findByCriteria(-1, -1, null, criterion);
	}

	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(final int firstResult,
			final int maxResults, final Order order,
			final Criterion... criterion) {
		Session session = (Session) entityManager.getDelegate();
		Criteria crit = session.createCriteria(entityClass);

		for (final Criterion c : criterion) {
			if (c != null) {
				crit.add(c);
			}
		}

		if (order != null) {
			crit.addOrder(order);
		}

		if (firstResult > 0) {
			crit.setFirstResult(firstResult);
		}

		if (maxResults > 0) {
			crit.setMaxResults(maxResults);
		}

		return (List<T>) crit.list();
	}

	/**
	 * Count element from build request with Criteria.
	 * 
	 * @param criterion
	 * @return
	 */
	protected int getCount(Criterion... criterion) {
		Session session = (Session) entityManager.getDelegate();
		Criteria crit = session.createCriteria(entityClass);
		crit.setProjection(Projections.rowCount());

		for (final Criterion c : criterion) {
			if (c != null) {
				crit.add(c);
			}
		}

		return ((Long) crit.list().get(0)).intValue();
	}

	/**
	 * switch to another persistence Unit.
	 * 
	 * @param persistentUnitName
	 */
	protected void setPersistentUnit(String persistentUnitName) {
		this.persistenceUnit = persistentUnitName;
		intializePersistentUnit(persistentUnitName);
	}

	/**
	 * @return the persistenceUnit
	 */
	protected String getPersistenceUnit() {
		return persistenceUnit;
	}

	/**
	 * Start a brand new transaction.
	 * 
	 * @return
	 */
	public void beginTransaction() {
		entityManager.getTransaction().begin();
	}

	/**
	 * Commit the transaction.
	 * 
	 * @param entityTransaction
	 */
	public void commit() {
		if (entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().commit();
		}
	}

	/**
	 * Roll back the transaction.
	 * 
	 * @param transaction
	 */
	public void rollBack() {

		if (entityManager.getTransaction().isActive()) {
			entityManager.getTransaction().rollback();
		}
	}

}
