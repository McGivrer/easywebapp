/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

import fr.mcgivrer.framework.web.engine.services.EasyService;
import fr.mcgivrer.framework.web.engine.services.EasyServiceGeneric;
import fr.mcgivrer.framework.web.engine.services.Service;

/**
 * <p>
 * Message management.
 * </p>
 * <p>
 * Load messages from properties file ( {@link ResourceBundle}). Then you can
 * access this service
 * </p>
 * 
 * @author FDELORME
 * 
 */
@EasyService(name="MessagesService",startlevel = 20)
public class Messages extends EasyServiceGeneric implements Service {
	/**
	 * internal instance for Singleton pattern.
	 */
	private static Messages instance = null;
	/**
	 * i18n Messages content.
	 */
	protected static ResourceBundle messages = null;

	/**
	 * Hidden constructor to obey to Singleton pattern.
	 */
	public Messages() {
		init();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.services.Service#init()
	 */
	@Override
	public void init() {
		messages = ResourceBundle.getBundle("messages");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.services.Service#getName()
	 */
	@Override
	public String getName() {
		return "i18n";
	}

	/**
	 * Retrieve a i18n message from <code>messages*.properties</code> files.
	 * 
	 * @param key
	 * @return
	 */
	private String getMessage(String key) {
		return (messages.containsKey(key) ? messages.getString(key) : String
				.format("key '%s' not defined", key));
	}

	/**
	 * Retrieve a i18n message from <code>messages*.properties</code> files and
	 * replace place-holders with <code>args</code> values.
	 * 
	 * @param key
	 * @return
	 */
	private String getMessage(String key, Object... args) {
		String message = null;
		if (messages.containsKey(key)) {
			message = messages.getString(key);
			message = String.format(message, args);
		} else {
			message = String.format("key '%s' not defined", key);
		}
		return message;
	}

	/**
	 * Set default local to set a specific language.
	 * 
	 * @param locale
	 */
	private void setMessagesLocale(Locale locale) {
		messages = ResourceBundle.getBundle("messages", locale);
	}

	public static String get(String key) {
		return Messages.getInstance().getMessage(key);
	}

	public static String get(String key, Object... args) {
		return Messages.getInstance().getMessage(key, args);
	}

	public static void setLocale(Locale locale) {
		Messages.getInstance().setMessagesLocale(locale);
	}

	public static Messages getInstance() {
		if (instance == null) {
			instance = new Messages();
		}
		return instance;
	}

}