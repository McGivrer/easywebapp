/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.authentication.impl;

import javax.servlet.ServletContext;

/**
 * Service Context object to provide external informations to Service
 * implementations.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class Context {
	/**
	 * Web context
	 */
	private ServletContext servletContext;

	/**
	 * Get the Servlet Context.
	 * 
	 * @return
	 */
	public ServletContext getServletContext() {
		return servletContext;
	}

	/**
	 * Set the ServletContext.
	 * 
	 * @param servletContext
	 */
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

}
