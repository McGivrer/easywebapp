/**
 * 
 */
package fr.mcgivrer.framework.web.engine.html.form;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import fr.mcgivrer.framework.web.engine.html.HtmlPlugin;
import fr.mcgivrer.framework.web.engine.html.elements.input.ButtonHtmlPlugin;
import fr.mcgivrer.framework.web.engine.html.elements.input.HiddenHtmlPlugin;
import fr.mcgivrer.framework.web.engine.html.elements.input.SubmitHtmlPlugin;
import fr.mcgivrer.framework.web.engine.html.elements.input.TextHtmlPlugin;
import fr.mcgivrer.framework.web.engine.html.elements.textarea.TextareaHtmlPlugin;

/**
 * <p>
 * Implementation for Basic Form rendering.
 * </p>
 * <p>
 * Initialize default provided <code>HtmlPlugin</code> for rendering
 * <code>Hidden</code>, <code>Text</code> and <code>Textarea</code> HTML
 * element.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class Form {

	private static final Logger logger = Logger.getLogger(Form.class);

	/**
	 * Map of Html Form element renderer.
	 */
	protected static Map<String, HtmlPlugin> plugins = new HashMap<String, HtmlPlugin>();

	/**
	 * Initialize Form with Html Plugin renderer.
	 */
	protected Form() {
		if (plugins.size() == 0) {
			plugins.put("text", new TextHtmlPlugin());
			logger.info("add TextHtmlPlugin");
			plugins.put("hidden", new HiddenHtmlPlugin());
			logger.info("add HiddenHtmlPlugin");
			plugins.put("textarea", new TextareaHtmlPlugin());
			logger.info("add TextareaHtmlPlugin");
			plugins.put("button", new ButtonHtmlPlugin());
			logger.info("add ButtonHtmlPlugin");
			plugins.put("submit", new SubmitHtmlPlugin());
			logger.info("add SubmitHtmlPlugin");
		}
	}
}
