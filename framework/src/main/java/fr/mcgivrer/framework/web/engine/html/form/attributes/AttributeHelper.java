/**
 * 
 */
package fr.mcgivrer.framework.web.engine.html.form.attributes;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * Helper to manipulate attribute value in session or Request.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class AttributeHelper {

	/**
	 * Retrieve the value named <code>attrName</code> from Http
	 * <code>request</code> from session if exists. if not, a
	 * <code>defaultValue</code> is set and the corresponding attribute name is
	 * stored in Http session.
	 * 
	 * @param request
	 * @param attrName
	 * @param defaultValue
	 * @return
	 */
	public static Object get(HttpServletRequest request, String attrName,
			Object defaultValue) {

		Object value = request.getParameter(attrName);
		if (value == null) {
			value = (String) request.getSession().getAttribute(attrName);
		}
		if (value == null) {
			value = defaultValue;
		}
		request.getSession().setAttribute(attrName, value);

		return value;
	}

}
