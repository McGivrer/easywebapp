/**
 * 
 */
package fr.mcgivrer.framework.web.engine.html.elements.input;

/**
 * @author mcgivrer
 *
 */
public class SubmitHtmlPlugin extends ButtonHtmlPlugin {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.html.elements.input.InputHtmlPlugin#
	 * getType()
	 */
	@Override
	public String getType() {
		return "submit";
	}
}
