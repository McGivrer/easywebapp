/**
 * 
 */
package fr.mcgivrer.framework.web.engine.configuration;

/**
 * @author mcgivrer
 * 
 */
public class Parameter implements Item {
	private String key;
	private String value;

	public Parameter(String key, String value) {
		this.setKey(key);
		this.value = value;
	}

	public boolean isOn() {
		return this.value != null
				&& "ON".equals(this.value.trim().toUpperCase());
	}

	public boolean isOff() {
		return (this.value == null)
				|| ((this.value != null) && "OFF".equals(this.value.trim()
						.toUpperCase()));
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format("Parameter[key=%s,value=%s]", key, value);
	}
}
