/**
 * 
 */
package fr.mcgivrer.framework.web.engine.models;

/**
 * @author mcgivrer
 * 
 */
public interface EEntity {
	public Object getField(String field);

	public void setField(String field, Object value);
}
