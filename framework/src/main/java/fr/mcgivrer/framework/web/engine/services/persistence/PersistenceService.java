/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.log4j.Logger;
import org.hibernate.MappingException;
import org.hibernate.ejb.Ejb3Configuration;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import fr.mcgivrer.framework.web.engine.configuration.Configuration;
import fr.mcgivrer.framework.web.engine.services.EasyService;
import fr.mcgivrer.framework.web.engine.services.EasyServiceGeneric;
import fr.mcgivrer.framework.web.engine.services.Service;
import fr.mcgivrer.framework.web.engine.services.persistence.exception.ORMException;

/**
 * <p>
 * Retrieve a session from Hibernate.
 * </p>
 * <p>
 * (from hibernate documentation)
 * </p>
 * <p>
 * This class auto detect all annotated entities and generate the hibernate
 * configuration with those classes from the file <code>db.properties</code>.
 * </p>
 * 
 * @author mcgivrer
 * 
 */
@EasyService(name = "PersistenceService", startlevel = 1)
public class PersistenceService extends EasyServiceGeneric implements Service {
	/**
	 * Standard Logger
	 */
	public static Logger logger = Logger.getLogger(PersistenceService.class);

	/**
	 * package to parse to retrieve annotated entities.
	 */
	private static final String FRAMEWORK_PCK_BASE_MODEL = "fr.mcgivrer.framework.web";

	/**
	 * Entity Manager Factory.
	 */
	public static Map<String, EntityManagerFactory> emf = new HashMap<String, EntityManagerFactory>();

	/**
	 * internal instance for the Singleton.
	 */
	private static PersistenceService instance = null;

	/**
	 * <p>
	 * Initialize the default persistence unit.
	 * </p>
	 */
	public PersistenceService() {
		try {
			init();
		} catch (Exception ex) {
			// Log exception!
			logger.fatal("unable to load application persistence unit", ex);

		}
	}

	/**
	 * <p>
	 * Initialize a <code>persistenceUnitName</code> with a specific
	 * configuration file (<code>configPropertiesFile</code>).
	 * </p>
	 * <p>
	 * EntityManegerFactory created for this persistence unit is added to the
	 * <code>emf</code> Map.
	 * </p>
	 * 
	 * @param persistenceUnitName
	 * @param configPropertiesFile
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws MappingException
	 * @throws ORMException
	 */
	@SuppressWarnings("deprecation")
	public static void initialize(
			String persistenceUnitName, String configPropertiesFile)
			throws IOException, ClassNotFoundException, MappingException,
			ORMException {
		Configuration.getInstance();
		Properties props = new Properties();
		if (configPropertiesFile != null) {
			try {
				InputStream is = PersistenceService.class
						.getResourceAsStream("/" + configPropertiesFile);
				props.load(is);
			} catch (IOException e) {
				logger.fatal(String.format(
						"Configuration file '%s' not found.",
						configPropertiesFile));
			}
		} else {
			props = Configuration
					.getPropertiesFromGroup(Configuration.CFG_DATABASE);
		}
		if (props != null && props.size() > 0) {

			// load JDBC driver on classpath.
			Class.forName(props
					.getProperty("hibernate.connection.driver_class"));

			// Entity Detection configuration
			ConfigurationBuilder reflectConfig = new ConfigurationBuilder()
					.addUrls(
							ClasspathHelper
									.forPackage(FRAMEWORK_PCK_BASE_MODEL))
					.addUrls(ClasspathHelper.forClass(Entity.class))
					.setScanners(new ResourcesScanner(),
							new TypeAnnotationsScanner());

			// prepare scan for annotated entities
			Ejb3Configuration jpaConfig = new Ejb3Configuration();

			// TODO: by default force the auto-commit mode to true. to be
			// removed
			// on next version.
			props.put("hibernate.connection.autocommit", "true");

			jpaConfig.addProperties(props);

			// Parse annotated entities and add these to the Hibernate
			// configuration component
			Reflections entitiesFilter = new Reflections(reflectConfig);
			for (Class<?> entity : entitiesFilter
					.getTypesAnnotatedWith(Entity.class)) {
				jpaConfig.addAnnotatedClass(entity);
			}

			// Configure Hibernate.
			EntityManagerFactory appEMF = jpaConfig.buildEntityManagerFactory();

			// Add this EntityManagerFactory to the list (in case of multiple
			// database
			// declaration).
			emf.put(persistenceUnitName, appEMF);

		} else {
			throw new ORMException("unable to find the '"
					+ configPropertiesFile + "' file");
		}
	}

	/**
	 * return an Entity manager for the persistUnitName.
	 * 
	 * @param persistUnitName
	 *            name of the persistent unit to retrieve.
	 * @return
	 * @throws ORMException
	 */
	public EntityManager getEntityManager(String persistsUnitName)
			throws ORMException {
		if (emf != null) {
			return emf.get(persistsUnitName).createEntityManager();
		} else {
			throw new ORMException("can not create EntityManager");
		}
	}

	/**
	 * return the default Entity manager for the application.
	 * 
	 * @return
	 * @throws ORMException
	 */
	public static EntityManager getEntityManager() throws ORMException {
		return getInstance().getEntityManager("application");
	}

	/**
	 * Retrieve the unique instance for HibernateUtil object.
	 * 
	 * @return
	 */
	public static PersistenceService getInstance() {
		if (instance == null) {
			instance = new PersistenceService();
		}
		return instance;
	}

	@Override
	public void init() throws Exception {
		initialize("application", null);

	}

	@Override
	public String getName() {
		return null;
	}
}