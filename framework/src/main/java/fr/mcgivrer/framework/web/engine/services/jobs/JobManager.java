/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.jobs;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import fr.mcgivrer.framework.web.engine.configuration.modules.routes.RoutesGroup;
import fr.mcgivrer.framework.web.engine.job.EasyJob;
import fr.mcgivrer.framework.web.engine.services.EasyService;
import fr.mcgivrer.framework.web.engine.services.EasyServiceGeneric;

/**
 * JobManager service to perform declared Jobs initialization and management.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
@EasyService(name="JobManagerService", startlevel=50)
public class JobManager extends EasyServiceGeneric {

	/**
	 * Type of event manage by the JobManager.
	 * 
	 * @author Frédéric Delorme<frederic.delorme@gmail.com>
	 * 
	 */
	public enum EVENTS {
		ONSTART, ONSTOP, TRIGGER
	}

	/**
	 * Jobs list managed by the application.
	 */
	private static Map<String, EasyJobGeneric> jobs = new HashMap<String, EasyJobGeneric>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.engine.services.Service#init()
	 */
	@Override
	public void init() throws Exception {

		ConfigurationBuilder config = new ConfigurationBuilder()
				.addUrls(
						ClasspathHelper
								.forPackage(RoutesGroup.FRAMEWORK_PCK_BASE_CONTROLLER))
				.addUrls(ClasspathHelper.forClass(EasyJob.class))
				.setScanners(new ResourcesScanner(),
						new TypeAnnotationsScanner(), new SubTypesScanner(),
						new MethodAnnotationsScanner());

		Reflections findJobs = new Reflections(config);

		// parse found methods
		Set<Class<?>> easyJobs = findJobs.getTypesAnnotatedWith(EasyJob.class);
		if (easyJobs.size() > 0) {
			for (Class<?> easyJobClass : easyJobs) {
				Object easyJob = easyJobClass.newInstance();
				EasyJobGeneric job = ((EasyJobGeneric) easyJob);
				jobs.put(job.name(),job);
				if (job.event().equals(EVENTS.ONSTART.toString())) {

					job.execute();

				}
			}
		}

	}
	
	
	/**
	 * On jobManager Stop, execute all <code>ONSTOP</code> event job.
	 */
	@Override
	public void stop() throws Exception {
		for(EasyJobGeneric job:jobs.values()){
			if(job.event().equals(JobManager.EVENTS.ONSTOP)){
				job.execute();
			}
		}
		super.stop();
	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.engine.services.Service#getName()
	 */
	@Override
	public String getName() {
		return "jobmanager";
	}

}
