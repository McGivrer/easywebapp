/**
 * 
 */
package fr.mcgivrer.framework.web.engine.configuration;

import org.apache.log4j.Logger;

/**
 * Read configuration group for parameterized information.
 * 
 * @author mcgivrer
 * 
 */
public abstract class ParameterGroup extends GenericGroup<Parameter> implements
		Group<Parameter> {
	private static Logger log = Logger.getLogger(ParameterGroup.class);

	public ParameterGroup() {
		log.info("instanciate " + this.getClass().getSimpleName()
				+ " configuration manager");
	}

	@Override
	public void readLine(String line) {
		String value="";
		String[] items = line.split("=");
		if((items.length>1) 
			&& (items[1]!=null) 
			&& !(items[1].equals("") )){
			value=items[1];
		}
		Parameter parameter = new Parameter(items[0], value);
		put(items[0], parameter);
		log.debug(String.format("Add the following parameter: %s",
				parameter.toString()));
	}

	@Override
	public abstract String getGroupName();

	@Override
	public void before() {

	}

	@Override
	public Object convertToProperty(Parameter t) {
		return t.getValue();
	}
}