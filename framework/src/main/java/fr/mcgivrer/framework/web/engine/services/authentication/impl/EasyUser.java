/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.authentication.impl;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import fr.mcgivrer.framework.web.engine.models.EasyEntity;
import fr.mcgivrer.framework.web.engine.services.authentication.UserGeneric;

/**
 * Internal Entity describing.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
@MappedSuperclass
public class EasyUser extends EasyEntity implements UserGeneric, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3978219413353218170L;

	@Size(min = 4, max = 12)
	private String userName;

	@Size(min = 1, max = 50)
	private String firstName;

	@Size(min = 1, max = 50)
	private String lastName;

	@Email
	private String email;

	@Size(min = 4, max = 30)
	private String password;

	public EasyUser() {
		super();
	}

	/**
	 * @param userName
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param password
	 */
	public EasyUser(String userName, String firstName, String lastName,
			String email, String password) {
		super();
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public String toString() {
		return String.format("%s (%s %s)", userName, firstName, lastName);
	}

}
