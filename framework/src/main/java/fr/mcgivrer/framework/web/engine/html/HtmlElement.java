/**
 * 
 */
package fr.mcgivrer.framework.web.engine.html;

import java.util.Map;

/**
 * Element HTML
 * 
 * @author Frédéric Delorme<fredric.delorme@gmail.com>
 * 
 */
public class HtmlElement {
	private String name = "";
	private String type = "";
	private Object value = "";
	private Map<String, String> attributes;

	/**
	 * @param name
	 * @param type
	 * @param value
	 */
	public HtmlElement(String name, String type, Object value,
			Map<String, String> attributes) {
		super();
		this.name = name;
		this.type = type;
		this.value = value;
		this.attributes = attributes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

}
