/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.render;

import java.util.HashMap;
import java.util.Map;

import fr.mcgivrer.framework.web.engine.services.Service;


/**
 * <p>
 * Render implementation base class for all next implementation.
 * </p>
 * <p>
 * For more information see <code>PageRenderSingleton.getInstance()</code> for
 * more information.
 * </p>
 * 
 * @see PageRenderService#getInstance()
 * 
 * @author Frédéric Delorme
 * 
 */
public abstract class GenericPageRender extends PageRenderService implements
		PageRender,Service {
	/**
	 * Action
	 */
	protected String action = "index";
	/**
	 * Default path for page templates.
	 */
	protected String baseTemplatePath = "/templates";
	/**
	 * Default Page template file extension (including the '.' dot).
	 */
	protected String defaultTemplateExtension = ".xhtml";

	/**
	 * Object Binding.
	 */
	public Map<String, Object> bindings = new HashMap<String, Object>();

	
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.easyweb.framework.render.PageRender#bind(java.lang.String,
	 * java.lang.Object)
	 */
	public void bind(String key, Object value) {
		bindings.put(key, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.easyweb.framework.render.PageRender#remove(java.lang.String)
	 */
	public void remove(String key) {
		bindings.remove(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.easyweb.framework.render.PageRender#flushBinding()
	 */
	public void flushBinding() {
		bindings = null;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.easyweb.framework.render.PageRender#getBindings()
	 */
	public Map<String, Object> getBindings() {
		return bindings;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.easyweb.framework.render.PageRender#setAction(java.lang.String
	 * )
	 */
	public void setAction(String pAction) {
		action = pAction;
	}

	/**
	 * Generate a default Error Page for Rendering engine composed with a
	 * <code>title</code> and a <code>message</code>. Attention: generated page
	 * is not "Browser" bullet proof.
	 * 
	 * @param title
	 * @param message
	 * @return
	 */
	public String getErrorPage(String title, String message) {
		return "<html>" + "<head>" + "<title>" + title + "</title>" + "</head>"
				+ "<body>" + "<div class=\"error\">" + "<p class=\"head\">"
				+ title + "</p>" + "<p class=\"message\">" + message + "</p>"
				+ "</div>" + "</body>" + "</html>";
	}

	@Override
	public void setTheme(String relativePathTheme) {
		// TODO must implement the Themes management.

	}

	@Override
	public String getTheme() {
		// TODO must implement the Themes management.
		return null;
	}
}
