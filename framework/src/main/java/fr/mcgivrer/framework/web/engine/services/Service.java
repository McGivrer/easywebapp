/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services;

/**
 * Declare a brand new Service.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public interface Service {

	/**
	 * <p>
	 * Service initialization.
	 * </p>
	 * <p>
	 * Called on Service instantiation.
	 * </p>
	 * 
	 * @throws Exception
	 */
	public void init() throws Exception;

	/**
	 * <p>
	 * Service Stop.
	 * </p>
	 * <p>
	 * Called on Service ending.
	 * </p>
	 * 
	 * @throws Exception
	 */
	public void stop() throws Exception;

	/**
	 * <p>
	 * Provide name of the service.
	 * </p>
	 * <p>
	 * Will be the name to manage the service in the Service Manager.
	 * </p>
	 * 
	 * @return
	 */
	public String getName();

}
