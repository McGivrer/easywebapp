/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.authentication;

import javax.servlet.http.HttpServletRequest;

import fr.mcgivrer.framework.web.engine.services.Service;

/**
 * @author FDELORME
 * 
 */
public interface Authentication<T extends UserGeneric> extends Service {

	public T authenticate(String username, String password);

	public T getUser(HttpServletRequest request) throws UnknownUserException;
}
