/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.authentication.impl;

import java.io.Serializable;

import fr.mcgivrer.framework.web.engine.services.authentication.UserGeneric;

/**
 * @author FDELORME
 * 
 */
public class BasicUser extends EasyUser implements UserGeneric, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5777122878920913219L;

	public static final int USER_USERNAME = 0;
	public static final int USER_PASSWORD = 1;
	public static final int USER_FIRSTNAME = 2;
	public static final int USER_LASTNAME = 3;
	public static final int USER_EMAIL = 4;

	/**
	 * 
	 */
	public BasicUser() {
		super();
	}

	/**
	 * @param userName
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param password
	 */
	public BasicUser(String userName, String firstName, String lastName,
			String email, String password) {
		super(userName, firstName, lastName, email, password);
	}

	public BasicUser(String[] userAttrs) {
		setUserName(userAttrs[USER_USERNAME]);
		setPassword(userAttrs[USER_PASSWORD]);
		setFirstName(userAttrs[USER_FIRSTNAME]);
		setLastName(userAttrs[USER_LASTNAME]);
		setEmail(userAttrs[USER_EMAIL]);
	}

}