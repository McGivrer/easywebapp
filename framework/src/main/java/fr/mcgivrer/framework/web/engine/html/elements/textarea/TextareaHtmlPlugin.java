/**
 * 
 */
package fr.mcgivrer.framework.web.engine.html.elements.textarea;

import fr.mcgivrer.framework.web.engine.html.HtmlPlugin;
import fr.mcgivrer.framework.web.engine.html.elements.FormElementHtmlPlugin;

/**
 * Generate a Texarea Html form element.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class TextareaHtmlPlugin extends FormElementHtmlPlugin implements
		HtmlPlugin {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.html.elements.FormElementHtmlPlugin#
	 * getElementType()
	 */
	@Override
	public String getStartElement() {

		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.html.elements.FormElementHtmlPlugin#
	 * getEndElement()
	 */
	@Override
	public String getEndElement() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.html.elements.FormElementHtmlPlugin#
	 * getTemplateElement()
	 */
	public String getTemplateElement() {
		return "@@label<br /><textarea @@attributes>@@value</textarea>";
	}

}
