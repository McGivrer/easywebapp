/**
 * 
 */
package fr.mcgivrer.framework.web.engine.configuration.modules.javascript;

import fr.mcgivrer.framework.web.engine.configuration.ParameterGroup;

/**
 * Javascript manager for EasyWebApp.
 * 
 * TODO Implement Javascript Service.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class JavascriptGroup extends ParameterGroup {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.configuration.ParameterGroup#getGroupName
	 * ()
	 */
	@Override
	public String getGroupName() {

		return "javascript";
	}

}
