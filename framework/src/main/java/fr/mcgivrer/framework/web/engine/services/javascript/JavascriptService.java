/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.javascript;

import java.util.Properties;

import fr.mcgivrer.framework.web.engine.configuration.Configuration;
import fr.mcgivrer.framework.web.engine.services.EasyService;
import fr.mcgivrer.framework.web.engine.services.EasyServiceGeneric;
import fr.mcgivrer.framework.web.engine.services.Service;

/**
 * <p>
 * The Javascript service.
 * </p>
 * <p>
 * The first goal is intended to generate the right header script tags to
 * integrate the Javascript API to your application.
 * </p>
 * See the [javascript] in the application.ini configuration file to set
 * /initialize your own needed API.
 * <p>
 * </p>
 * <p>
 * Defaults javascript API are <code>Jquery-1.7.2.js</code> and the
 * <code>easywebapp.js</code> base mechanism.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
@EasyService(name="JavascriptService", startlevel = 40)
public class JavascriptService extends EasyServiceGeneric implements Service {
	/**
	 * Internal Service properties.
	 */
	private static Properties javascriptFiles = new Properties();
	/**
	 * the generated header part containing the <code>&lt;script&gt;</code> tags
	 * based on props list.
	 */
	private static String headerJavascript = "";

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.engine.services.Service#init()
	 */
	@Override
	public void init() throws Exception {
		javascriptFiles = Configuration.getPropertiesFromGroup("javascript");
		StringBuffer sb = new StringBuffer("");
		while (javascriptFiles.propertyNames().hasMoreElements()) {
			String propertyName = (String) javascriptFiles.propertyNames().nextElement();
			sb.append(String
					.format("<script name=\"%s\" type=\"text/javascript\" src=\"%s\"></script>",
							propertyName, javascriptFiles.getProperty(propertyName)));

		}
		headerJavascript = sb.toString();

	}

	public static String toHtmlHeader() {
		if (headerJavascript != null) {
			return headerJavascript;
		}
		return "<!-- Javascript API undefined in configuration file : see application.ini -->";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.engine.services.Service#getName()
	 */
	@Override
	public String getName() {
		return "javascript";
	}

}
