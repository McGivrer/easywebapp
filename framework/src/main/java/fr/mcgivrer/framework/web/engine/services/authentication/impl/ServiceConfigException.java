/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.authentication.impl;

/**
 * Exception thrown in case of Service initialization error.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com
 * 
 */
public class ServiceConfigException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3005762076096100266L;

	/**
	 * 
	 */
	public ServiceConfigException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public ServiceConfigException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public ServiceConfigException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ServiceConfigException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
