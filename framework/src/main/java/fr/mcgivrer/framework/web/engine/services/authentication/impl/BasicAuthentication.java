/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.authentication.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import fr.mcgivrer.framework.web.engine.configuration.PropertiesLoader;
import fr.mcgivrer.framework.web.engine.services.EasyService;
import fr.mcgivrer.framework.web.engine.services.EasyServiceGeneric;
import fr.mcgivrer.framework.web.engine.services.authentication.Authentication;
import fr.mcgivrer.framework.web.engine.services.authentication.UnknownUserException;

/**
 * <p>
 * Basic User Authentication Service for web Application.
 * </p>
 * <p>
 * Usage:
 * </p>
 * <p>
 * In an Application Controller (Extending AppController) create a
 * <code>security(HttpServletRequest request)</code> method like the following
 * sample:
 * 
 * <pre>
 * User user = null;
 * 
 * &#064;BeforeControl
 * public void security(HttpServletRequest request) {
 * 	user = BasicAuthentication.getUser(request);
 * 	BasicAuthentication.setUserConnected(request, user);
 * }
 * </pre>
 * 
 * </p>
 * 
 * @author FDELORME
 * 
 */
@EasyService(name = "BasicAuthenticationService", startlevel = 10)
public class BasicAuthentication extends EasyServiceGeneric implements
		Authentication<BasicUser> {
	/**
	 * String key to retrieve connected user in session.
	 */
	public static final String CONNECTED_USER = "CONNECTED_USER";

	/**
	 * Logger for BasicAuthentication.
	 */
	private static Logger log = Logger.getLogger(BasicAuthentication.class);
	/**
	 * List of loaded users from <code>users.properties</code>.
	 */
	Map<String, BasicUser> users = new HashMap<String, BasicUser>();

	/**
	 * specific constructor to initialize users list.
	 */
	public BasicAuthentication() {
		try {
			init();
		} catch (ServiceConfigException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.engine.services.EasyServiceGeneric#init()
	 */
	public void init() throws ServiceConfigException {
		// Load users from the properties file.
		Properties usersFile = PropertiesLoader.load("users.properties");
		String value = "";
		if (usersFile != null) {
			for (Object propKey : usersFile.keySet()) {
				value = (String) usersFile.getProperty((String) propKey);
				String[] userAttrs = ("" + (String) propKey + ":" + value)
						.split(":");
				if (userAttrs.length == 5) {
					BasicUser user = new BasicUser(userAttrs);
					users.put((String) propKey, user);
				} else {
					log.error("User on line " + propKey + "=[" + value
							+ "] is not readable");
				}
			}
		} else {
			throw new ServiceConfigException("file 'user.properties' not found");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.services.authentication.Authentication#getUser
	 * (javax.servlet.http.HttpServletRequest)
	 */
	public BasicUser getUser(HttpServletRequest request)
			throws UnknownUserException {
		BasicUser user = null;
		if (request.getSession().getAttribute(CONNECTED_USER) != null) {
			user = (BasicUser) request.getSession()
					.getAttribute(CONNECTED_USER);
		} else {
			throw new UnknownUserException("User not connected.");
		}
		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.services.authentication.Authentication#authenticate
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public BasicUser authenticate(String username, String password)
			throws SecurityException {
		if (users.size() > 0) {
			BasicUser user = users.get(username);
			if (user.getPassword().equals(password)) {
				return user;
			} else {
				throw new SecurityException(
						"User and/or password are not known by application");
			}
		} else {
			throw new SecurityException("No users list initialized !");
		}
	}

	/**
	 * Set reference to connected <code>user</code> into session.
	 * 
	 * @param request
	 * @param user
	 */
	public void setUserConnected(HttpServletRequest request, BasicUser user) {
		request.getSession().setAttribute(CONNECTED_USER, user.getUserName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.services.EasyServiceGeneric#getName()
	 */
	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

}