/**
 * 
 */
package fr.mcgivrer.framework.web.engine.debug;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * Debug zone Manager.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class DebugMessage {
	/**
	 * default logger for this component.
	 */
	private static Logger logger = Logger.getLogger(DebugMessage.class);

	/**
	 * Display information about Dispatch and Route.
	 * 
	 * @param resp
	 * @param route
	 */
	public static void show(HttpServletResponse resp, String debugLines) {
		try {
			Writer writer = resp.getWriter();
			resp.setContentType("text/html");
			writer.write(String
					.format("<div id=\"debug-view\" class=\"debug\">"
							+ "<p><a class='button' title=\"Hide debug zone\" href=\"#\">x</a>"
							+ "&nbsp;Debug zone</p>" + "<pre>%s</pre>"
							+ "</div>", debugLines));

		} catch (IOException e) {
			logger.fatal("Error on writing the HttpResponse.", e);
		}
	}
}
