/**
 * 
 */
package fr.mcgivrer.framework.web.engine.job;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import fr.mcgivrer.framework.web.engine.services.jobs.JobManager;

/**
 * <p>
 * Job annotation
 * </p>
 * <p>
 * Used to create a brand new Job which can be schedule or executed one shot.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EasyJob {
	/**
	 * Name for this particular Job.
	 * 
	 * @return
	 */
	String name() default "job";

	/**
	 * This is the trigger for the event.
	 * 
	 * @see org.quartz.Trigger
	 * 
	 * @return
	 */
	String trigger() default "";

	/**
	 * <p>
	 * Schedule for this Job.
	 * </p>
	 * <p>
	 * default is "now" for one execution now. Can specify another schedule
	 * (@see Quartz API).
	 * </p>
	 * 
	 * @return
	 */
	JobManager.EVENTS event() default JobManager.EVENTS.ONSTART;
}
