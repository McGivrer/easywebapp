/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.render.exceptions;

/**
 * @author frederic
 * 
 */
public class PageRenderException extends Exception {

	public PageRenderException() {
		super();
	}

	public PageRenderException(String message, Throwable cause) {
		super(message, cause);
	}

	public PageRenderException(String message) {
		super(message);
	}

	public PageRenderException(Throwable cause) {
		super(cause);
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6426247202337430033L;

}
