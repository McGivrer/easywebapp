/**
 * 
 */
package fr.mcgivrer.framework.web.engine.configuration.modules.render;

import fr.mcgivrer.framework.web.engine.configuration.ParameterGroup;

/**
 * @author fdelorme
 * 
 */
public class RenderGroup extends ParameterGroup {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.configuration.ParameterGroup#getGroupName
	 * ()
	 */
	@Override
	public String getGroupName() {
		return "render";
	}

}