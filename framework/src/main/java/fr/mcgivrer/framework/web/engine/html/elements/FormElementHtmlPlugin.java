/**
 * 
 */
package fr.mcgivrer.framework.web.engine.html.elements;

import java.util.HashMap;
import java.util.Map;

import fr.mcgivrer.framework.web.engine.html.HtmlElement;
import fr.mcgivrer.framework.web.engine.html.HtmlPlugin;
import fr.mcgivrer.framework.web.engine.services.i18n.Messages;

/**
 * <p>
 * <code>FormElementHtmlPlugin</code> is the default class to render Html
 * Element Form.
 * </p>
 * <p>
 * Provide all basic beahviour to perform rendering of Html Element, based on
 * templating.
 * </p>
 * <p>
 * These classes inheriting from <code>FormElementHtmlPlugin</code> would be
 * call from groovy template.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public abstract class FormElementHtmlPlugin implements HtmlPlugin {

	protected StringBuffer outputHtml = new StringBuffer("");

	protected Map<String, Object> attributes = new HashMap<String, Object>();

	protected HtmlElement element = null;

	protected abstract String getStartElement();

	public String getTemplateElement() {
		return "";
	}

	public boolean getShowElementDecoration() {
		return true;
	}

	protected abstract String getEndElement();

	/**
	 * If provided in <code>attributes</code>, the <code>labelMEssageKey</code>
	 * will provide the i18n message to output the label.
	 * 
	 * @return
	 */
	public String writeLabel() {
		String label = "";
		if (attributes.containsKey("labelMessageKey")) {
			label = String.format(
					"<label for=\"%s\" %s>%s</label>",
					element.getName(),
					(Messages.get((String) attributes.get("labelMessageKey")
							+ ".help") != null ? "title=\""
							+ Messages.get((String) attributes
									.get("labelMessageKey") + ".help") + "\""
							: ""), Messages.get((String) attributes
							.get("labelMessageKey")));
		}
		return label;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.html.HtmlPlugin#beforeElementDecorator()
	 */
	public String beforeElementDecorator() {
		return "<li>";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.html.HtmlPlugin#afterElementDecorator()
	 */
	public String afterElementDecorator() {
		return "</li>";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.html.HtmlPlugin#init(java.lang.Object,
	 * java.util.Map)
	 */
	@Override
	public HtmlPlugin init(HtmlElement element) {
		attributes.put("name", element.getName());
		if (element.getAttributes() != null) {
			attributes.putAll(element.getAttributes());
		}
		this.element = element;
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.engine.html.HtmlPlugin#render()
	 */
	@Override
	public String render() {
		// Add Name
		outputHtml
				.append("name=\"" + attributes.get("name").toString() + "\" ");
		// Add Id
		if (attributes.get("id") == null) {
			outputHtml.append(String.format("%s=\"%s\" ", "id",
					attributes.get("name").toString()));
		}
		// Add value
		if (element.getValue() != null) {
			outputHtml.append(String.format("%s=\"%s\" ", "value",
					element.getValue()));
		}

		// render attributes
		for (String key : attributes.keySet()) {
			if (!key.equals("labelMessageKey")) {
				outputHtml.append(String.format("%s=\"%s\" ", key, attributes
						.get(key).toString()));
			}
		}

		// Generate label.
		String label = writeLabel();
		// Prepare value
		String value = "";
		// is a template is define for such Form element ?
		
		if (getTemplateElement() != null && !getTemplateElement().equals("")) {
		
			// Yes, template is set, replace placaholders.
			
			value = getTemplateElement();
			value = value.replaceAll("(?:@@attributes)", outputHtml.toString());
			value = value.replaceAll("(?:@@value)",
					(element.getValue() != null ? element.getValue().toString()
							: ""));
			value = value.replaceAll("(?:@@label)", label);
		} else {
			
			// No, so use standard getStartElement() and getEndElement()
			
			value = label
					+ String.format(getStartElement(), outputHtml.toString(),
							element.getValue()) + getEndElement();
		}
		return value;
	}
}
