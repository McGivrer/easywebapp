/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.jobs;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;

/**
 * Initialize the JobManager and instantiate all detected Jobs.
 * 
 * @see JobManager
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class JobManagerServlet extends HttpServlet {

	/**
	 * generated SerialUID
	 */
	private static final long serialVersionUID = 9156679534779347092L;

	/**
	 * Internal Logger
	 */
	private final Logger logger = Logger.getLogger(JobManagerServlet.class);

	/**
	 * JobManager
	 */
	private JobManager jobManager = new JobManager();

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		try {
			jobManager.init();
		} catch (Exception e) {
			logger.fatal("JobManager was not initialized", e);
		}
	}

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#destroy()
	 */
	@Override
	public void destroy() {
		super.destroy();
		try {
			jobManager.stop();
		} catch (Exception e) {
			logger.fatal("JobManager can not stop Jobs", e);
		}
	}
	
	

}
