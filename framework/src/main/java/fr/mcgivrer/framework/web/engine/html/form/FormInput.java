package fr.mcgivrer.framework.web.engine.html.form;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import fr.mcgivrer.framework.web.engine.models.EEntity;
import fr.mcgivrer.framework.web.engine.models.EasyEntity;

/**
 * <p>
 * All Input field for HTML form will be generated by FormInput.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class FormInput extends Form {

	private static final Logger logger = Logger.getLogger(FormInput.class);

	protected FormInput() {
		super();
	}

	/**
	 * TODO Will retrieve the named <code>entityType</code>, instantiate the
	 * entity and return it.
	 * 
	 * @param string
	 * @return
	 */
	public static EEntity getEntity(HttpServletRequest req, String entityType,
			Class<?> type) {
		try {
			EEntity entity = (EasyEntity) type.newInstance();
			Enumeration<String> attrIter = req.getAttributeNames();
			while (attrIter.hasMoreElements()) {
				String attrName = attrIter.nextElement();
				String attrValue = (String) req.getAttribute(attrName);
				logger.debug(String.format(
						"map atribute %s to entity with value %s", attrName,
						attrValue));
			}
			Enumeration<String> paramsIter = req.getParameterNames();
			while (paramsIter.hasMoreElements()) {
				String paramsName = attrIter.nextElement();
				String paramsValue = (String) req.getAttribute(paramsName);
				logger.debug(String.format(
						"map parameter %s to entity with value %s", paramsName,
						paramsValue));
			}

			return entity;

		} catch (InstantiationException e) {
			logger.fatal(String.format(
					"unable to instatiate Entity from form: %s", e.getMessage()));

		} catch (IllegalAccessException e) {
			logger.fatal(String.format(
					"unable to instatiate Entity from form: %s", e.getMessage()));

		}
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public static FormInput getInstace() {
		return new FormInput();
	}

}
