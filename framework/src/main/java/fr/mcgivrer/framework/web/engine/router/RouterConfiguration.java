/**
 * 
 */
package fr.mcgivrer.framework.web.engine.router;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import fr.mcgivrer.framework.web.engine.configuration.Configuration;
import fr.mcgivrer.framework.web.engine.configuration.ConfigurationException;

/**
 * Configuration loader for the Dispatcher module.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class RouterConfiguration {
	/**
	 * default logger for this component.
	 */
	private static Logger logger = Logger.getLogger(RouterConfiguration.class);

	/**
	 * Configuration manager.
	 */
	private static Configuration configuration = null;

	/**
	 * Path to configuration file.
	 */
	private static String cfgFilePath = "";

	public RouterConfiguration(String path) {
		readFile(path);
	}

	/**
	 * Read full Configuration file for framework.
	 * 
	 * @param configFullFilePath
	 */
	public void readFile(String configFullFilePath) {
		if (configuration == null && configFullFilePath != null
				&& !configFullFilePath.equals("")) {
			try {
				setCfgFilePath(configFullFilePath);
				configuration = Configuration.getInstance().read(
							configFullFilePath);
			} catch (ConfigurationException e) {
				logger.fatal("Error on configuration file.", e);
			}
		}
	}

	/**
	 * Is request is a reload order (url contains <code>reload-config</code>)?
	 * 
	 * @param req
	 * @throws ServletException
	 */
	public void isConfigReloadRequest(HttpServletRequest req)
			throws ServletException {
		if (req.getParameter("reload-config") != null && cfgFilePath != null
				&& !cfgFilePath.equals("")) {
			logger.info(String.format("reload configuration %s file.",
					cfgFilePath));
			readFile(cfgFilePath);
		}
	}

	/**
	 * @return the cfgFilePath
	 */
	public static String getCfgFilePath() {
		return cfgFilePath;
	}

	/**
	 * Set the Config file path.
	 * @param cfgFilePath
	 *            the cfgFilePath to set
	 */
	public static void setCfgFilePath(String cfgFilePath) {
		RouterConfiguration.cfgFilePath = cfgFilePath;
	}
}
