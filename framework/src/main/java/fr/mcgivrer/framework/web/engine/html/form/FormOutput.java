/**
 * 
 */
package fr.mcgivrer.framework.web.engine.html.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import fr.mcgivrer.framework.web.engine.html.HtmlElement;
import fr.mcgivrer.framework.web.engine.html.HtmlPlugin;

/**
 * <p>
 * <code>FormOutput</code> is the generic implementation to provide Form
 * rendering.
 * </p>
 * <p>
 * Introduce some HTML decoration elements on the <code>before</code> and
 * <code>after</code> Form rendering.
 * </p>
 * <p>
 * Rendering
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class FormOutput extends Form {

	private static final Logger logger = Logger.getLogger(FormOutput.class);

	/**
	 * Output component to produce form
	 */
	private static StringBuffer form;

	/**
	 * Map of form element for this instance.
	 */
	private static List<HtmlElement> elements = new ArrayList<HtmlElement>();

	/**
	 * Initialize Form with Html Plugin renderer.
	 */
	protected FormOutput() {
		super();
	}

	/**
	 * Start a brand new form with <code>action </code>and some
	 * <code>args</code>
	 * 
	 * @param action
	 * @param args
	 */
	public static void begin(String action, Map<String, String> attributes) {

		form = new StringBuffer();
		elements.clear();
		form.append("<form action=\"" + action + "\"");
		if (attributes != null && attributes.size() > 0) {
			for (String key : attributes.keySet()) {
				form.append(String.format("%s=\"\" ", key, attributes.get(key)));
			}
		}
		form.append("\">");
		form.append(beforeFormDecorator());
	}

	/**
	 * Start a brand new form with <code>action </code>and some
	 * <code>args</code>
	 * 
	 * @param action
	 * @param args
	 */
	public static void begin(String action) {
		begin(action, null);
	}

	/**
	 * <p>
	 * Add a new Input to the HTML form. <code>type</code>, <code>name</code>
	 * and <code>value</code> are the main attributes for this html element.
	 * </p>
	 * <p>
	 * If provided <code>labelMessageKey</code> id the ...message key for an
	 * i18n label.
	 * </p>
	 * 
	 * @param name
	 * @param type
	 * @param value
	 * @param attributes
	 * @param labelMessageKey
	 */
	public static void add(String name, String type, Object value,
			String labelMessageKey) {
		HashMap<String, String> attributes = new HashMap<String, String>();
		if (labelMessageKey != null) {
			attributes.put("labelMessageKey", labelMessageKey);
		}
		add(name, type, value, attributes);
	}

	/**
	 * <p>
	 * Add a new Input to the HTML form. <code>type</code>, <code>name</code>
	 * and <code>value</code> are the main attributes for this html element.
	 * </p>
	 * <p>
	 * If provided <code>labelMessageKey</code> id the ...message key for an
	 * i18n label.
	 * </p>
	 * 
	 * @param name
	 * @param type
	 * @param value
	 * @param attributes
	 * @param labelMessageKey
	 */
	public static void add(String name, String type, Object value,
			String labelMessageKey, Map<String, String> attributes) {
		if (labelMessageKey != null) {
			attributes.put("labelMessageKey", labelMessageKey);
		}
		HtmlElement element = new HtmlElement(name, type, value, attributes);
		elements.add(element);
	}

	/**
	 * Add a new Input to the HTML form. type name and value are the main
	 * attributes for this html element.
	 * 
	 * @param name
	 * @param type
	 * @param value
	 * @param attributes
	 */
	public static void add(String name, String type, Object value,
			Map<String, String> attributes) {
		add(name, type, value, null, attributes);
	}

	/**
	 * Add an element to the form (with no specific attribute).
	 * 
	 * @param name
	 * @param type
	 * @param value
	 */
	public static void add(String name, String type, String value) {
		HtmlElement element = new HtmlElement(name, type, value, null);
		elements.add(element);
	}

	/**
	 * End of form.Generate Form content at end() call.
	 * 
	 * @return
	 */
	public static String end() {

		for (HtmlElement element : elements) {
			HtmlPlugin plugin = plugins.get(element.getType());
			if (plugin != null) {
				plugin.init(element);

				if (plugin.getShowElementDecoration()) {
					form.append(plugin.beforeElementDecorator());
				}

				form.append(plugin.render());

				if (plugin.getShowElementDecoration()) {
					form.append(plugin.afterElementDecorator());
				}
			} else {
				logger.error(String.format(
						"HtmlPlugin '%s' type does not exists.",
						element.getType()));
			}
		}
		form.append(afterFormDecorator());
		form.append("</form>");
		return form.toString();
	}

	/**
	 * Default before Form decorator.
	 * 
	 * @return
	 */
	protected static String beforeFormDecorator() {
		return "<ul>";
	}

	/**
	 * Default after Form decorator.
	 * 
	 * @return
	 */
	protected static String afterFormDecorator() {
		return "</ul>";
	}

	/**
	 * 
	 * @return
	 */
	public static FormOutput getInstance() {
		return new FormOutput();
	}
}
