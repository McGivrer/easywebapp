/**
 * 
 */
package fr.mcgivrer.framework.web.engine.html.elements.input;


/**
 * <p>
 * Generate a Hidden HTML form element.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class HiddenHtmlPlugin extends InputHtmlPlugin{

	@Override
	public String getType() {
		return "hidden";
	}

	@Override
	public boolean getShowElementDecoration() {
		return false;
	}
}
