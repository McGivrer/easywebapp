/**
 * 
 */
package fr.mcgivrer.framework.web.engine.configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.hsqldb.lib.StringUtil;

/**
 * Generic Group configuration items implementation. Provide basics of the Items
 * manegement: Map, put, get
 * 
 * @author mcgivrer
 * 
 */
public abstract class GenericGroup<T> implements Group<T> {

	/**
	 * Map of element managed by this Group implementation.
	 */
	protected Map<String, T> list = new HashMap<String, T>();

	/**
	 * Add an item to the Map.
	 * 
	 * @param name
	 * @param item
	 */
	protected void put(String name, T item) {
		list.put(name, item);
	}

	/**
	 * retrieve an item from the Map.
	 * 
	 * @param name
	 */
	public T get(String name) {
		return list.get(name);
	}

	/**
	 * return all items from HashMap.
	 * 
	 * @return
	 */
	protected Map<String, T> getItems() {
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.configuration.Group#read(java.io.BufferedReader
	 * )
	 */
	public BufferedReader read(BufferedReader bufRead) throws IOException {
		String line = "";

		while ((line = bufRead.readLine()) != null) {
			if (line.trim().startsWith("[")) {
				bufRead.reset();
				break;
			}
			if (!StringUtil.isEmpty(line) && !line.trim().startsWith("#")) {
				readLine(line);
			}
			bufRead.mark(0);
		}
		return bufRead;
	}

	/**
	 * return the Group of values for the corresponding group.
	 */
	public Map<String, T> getAll() {
		return list;
	}

	/**
	 * Converts all item into Properties object.
	 */
	public Properties getAllProps() {
		Properties props = new Properties();
		for (String item : list.keySet()) {
			props.put(item, convertToProperty(list.get(item)));
		}
		return props;
	}

	/**
	 * convert an T item into a property value.
	 */
	public abstract Object convertToProperty(T t);
}