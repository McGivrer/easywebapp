/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.logger;

import java.io.File;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

import fr.mcgivrer.framework.web.engine.services.EasyService;
import fr.mcgivrer.framework.web.engine.services.EasyServiceGeneric;
import fr.mcgivrer.framework.web.engine.services.Service;

/**
 * Logger Service. can be initialize with Log4JInitServlet.
 * 
 * @see Log4JInitServlet
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
@EasyService(name = "LoggerService", startlevel = 1)
public class LoggerService extends EasyServiceGeneric implements Service {

	/**
	 * Initialization of the service.
	 * 
	 * @param configFilePath
	 * @throws Exception
	 */
	public void init(String configFilePath) throws Exception {
		if (configFilePath == null) {
			System.err
					.println("*** No log4j-config-file init param, so initializing log4j with BasicConfigurator");
			BasicConfigurator.configure();
		} else {
			readConfiguration(configFilePath);
		}

	}

	/**
	 * Initialize with default path.
	 */
	public void init() throws Exception {
		String appPath = this.getClass().getResource("/").getPath()
				+ "WEB-INF/configuration/log4j.xml";
		init(appPath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.engine.services.Service#stop()
	 */
	@Override
	public void stop() throws Exception {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.services.Service#getName()
	 */
	@Override
	public String getName() {
		return "logger";
	}

	/**
	 * @param configFilePath
	 */
	public void readConfiguration(String configFilePath) {
		File configFile = new File(configFilePath);
		if (configFile.exists()) {
			System.out.println("Initializing log4j with: " + configFilePath);
			PropertyConfigurator.configure(configFilePath);
		} else {
			System.err
					.println("*** "
							+ configFilePath
							+ " file not found, so initializing log4j with BasicConfigurator");
			BasicConfigurator.configure();
		}
	}
}
