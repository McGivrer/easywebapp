/**
 * 
 */
package fr.mcgivrer.framework.web.engine.configuration.modules.routes;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Set;

import org.apache.log4j.Logger;
import org.reflections.Reflections;

import fr.mcgivrer.framework.web.engine.configuration.GenericGroup;
import fr.mcgivrer.framework.web.engine.configuration.Group;
import fr.mcgivrer.framework.web.engine.controls.Controller;
import fr.mcgivrer.framework.web.engine.controls.annotations.AfterControl;
import fr.mcgivrer.framework.web.engine.controls.annotations.BeforeControl;

/**
 * Routes manager. provide the Routes identified in the Project's existing
 * Controllers, and read configuration from the "<code>routes</code>" group in
 * the configuration file.
 * 
 * @author mcgivrer
 * 
 */
public class RoutesGroup extends GenericGroup<Route> implements Group<Route> {

	/**
	 * defautl path where Controllers are created.
	 */
	public static final String FRAMEWORK_PCK_BASE_CONTROLLER = "fr.mcgivrer.framework.web.application";

	/**
	 * Logger.
	 */
	private static Logger log = Logger.getLogger(RoutesGroup.class);

	public RoutesGroup() {
		log.info("Instantiate RoutesGroup configuration manager");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.configuration.Group#getGroupName()
	 */
	@Override
	public String getGroupName() {
		return "routes";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.configuration.Group#readLine(java.lang.String)
	 */
	@Override
	public void readLine(String line) {
		Route route = new Route();
		String[] elements = line.trim().replace("\t\t", "\t").split("\t");
		int i = 0;
		// retrieve fields for Route.
		for (String element : elements) {
			if (element != null && !element.equals("")) {
				switch (i) {
				case 0:
					route.setMode(element.trim());
					break;
				case 1:
					route.setPattern(element.trim());
					break;
				case 2:
					route.setControllerMethodPath(element.trim());
					break;
				default:
					break;
				}
				i++;
			}
		}
		String[] items = route.getControllerMethodPath().split(".");
		System.out.println("Controller+method: " + items);
		/*
		 * TODO implement instantiation of matching controller. (see readRoutes
		 * bellow).
		 */

		// add identified route to list.
		put(route.getPattern(), route);
		log.debug("Add route " + route.toString());
		// next route !

	}

	/**
	 * parse the application package to detect all Application controller
	 * (inheriting the <code>AppController</code> class) and add them to the
	 * <code>routes</code> list if those are not already added through
	 * <code>routes.ini</code> file.
	 */
	public void before() {
		log.info("Auto-detection of Controller in this application scope :"
				+ FRAMEWORK_PCK_BASE_CONTROLLER);

		Reflections reflections = new Reflections(FRAMEWORK_PCK_BASE_CONTROLLER);

		Set<Class<? extends Controller>> classes = reflections
				.getSubTypesOf(Controller.class);
		for (Class<? extends Controller> clazz : classes) {
			log.debug("Controller " + clazz.getName() + " detected.");
			// Controller controller;
			// controller = clazz.newInstance();
			Method[] methods = clazz.getMethods();
			for (Method method : methods) {
				// If method is public then parse parameters.
				if (Modifier.isPublic(method.getModifiers())
						&& method.getDeclaringClass().equals(clazz)
						&& method.getAnnotation(BeforeControl.class)==null
						&& method.getAnnotation(AfterControl.class)==null ) {


					Route route = new Route();
					route.setMethod(method);
					route.setController(clazz);
					log.debug("Controller " + clazz.getSimpleName()
							+ " : methods " + method.getName() + " detected.");
					route.setControllerMethodPath(clazz.getSimpleName()
							.toLowerCase()
							+ "."
							+ method.getName().toLowerCase());
					route.setPattern("/" + clazz.getSimpleName().toLowerCase()
							+ "/" + method.getName().toLowerCase());
					Type[] types = method.getGenericParameterTypes();
					if (types.length != 0) {
						log.debug("method " + method.getName()
								+ "have following parameters:");
						String params = "";
						for (Type type : types) {
							if (!params.equals("")) {
								params += ",";
							}
							params += type.getClass().getName();
						}
						log.debug(params);
					}
					list.put(route.getPattern(), route);
					log.debug("Add route " + route.toString());
				}
			}

		}

	}

	@Override
	public Object convertToProperty(Route t) {
		return t.toString();
	}
}
