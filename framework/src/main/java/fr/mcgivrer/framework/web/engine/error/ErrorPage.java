/**
 * 
 */
package fr.mcgivrer.framework.web.engine.error;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import fr.mcgivrer.framework.web.engine.services.i18n.Messages;
import fr.mcgivrer.framework.web.engine.services.render.PageRenderService;
import fr.mcgivrer.framework.web.engine.services.render.exceptions.PageRenderException;

/**
 * Error Page manager.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class ErrorPage {
	/**
	 * default logger for this component.
	 */
	private static Logger logger = Logger.getLogger(ErrorPage.class);

	/**
	 * Display on distant browser an encapsulated error page.
	 * 
	 * @param req
	 * @param resp
	 * @param message
	 * @param e
	 */
	public static void display(HttpServletRequest req,
			HttpServletResponse resp, String message, Exception e) {
		Map<String, Object> binds = new HashMap<String, Object>();

		try {
			resp.setContentType("text/html");

			binds.put("message", message);
			binds.put("exception", e);
			// binds.put("sources",e.get);
			binds.put("messages", Messages.getInstance());
			binds.put("url", req.getRequestURL().toString());

			String errorPage = PageRenderService.getInstance().getRender()
					.render(binds, "errors/error.xhtml");

			Writer writer = resp.getWriter();
			writer.write(errorPage);

		} catch (IOException ioe) {
			logger.fatal("Error on writing the HttpResponse.", ioe);
		} catch (PageRenderException pre) {
			logger.fatal("Error on rendering error page", pre);
		}
	}
}
