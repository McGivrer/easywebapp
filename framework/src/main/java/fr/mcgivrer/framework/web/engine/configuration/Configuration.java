/**
 * 
 */
package fr.mcgivrer.framework.web.engine.configuration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import fr.mcgivrer.framework.web.engine.configuration.modules.database.DatabaseGroup;
import fr.mcgivrer.framework.web.engine.configuration.modules.debug.DebugGroup;
import fr.mcgivrer.framework.web.engine.configuration.modules.render.RenderGroup;
import fr.mcgivrer.framework.web.engine.configuration.modules.routes.RoutesGroup;

/**
 * Configuration management.
 * 
 * @author FDELORME
 * 
 */
public class Configuration {

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(Configuration.class);

	/**
	 * internal instance.
	 */
	private static Configuration instance = null;

	/**
	 * Configuration Groups
	 */
	private static Map<String, Group<? extends Item>> groups = new HashMap<String, Group<? extends Item>>();

	public static final String CFG_DEBUG = "debug";
	public static final String CFG_RENDER = "render";
	public static final String CFG_DATABASE = "database";
	public static final String CFG_ROUTES = "routes";

	/**
	 * Initialize the configuration loader.
	 */
	private Configuration() {

		groups.put(CFG_DEBUG, new DebugGroup());
		groups.put(CFG_RENDER, new RenderGroup());
		groups.put(CFG_DATABASE, new DatabaseGroup());
		groups.put(CFG_ROUTES, new RoutesGroup());

		for (String groupName : groups.keySet()) {
			Group<? extends Item> group = groups.get(groupName);
			group.before();
		}
	}

	/**
	 * 
	 * @param fullPathConfigurationFile
	 * @return
	 */
	public Configuration read(String fullPathConfigurationFile)
			throws ConfigurationException {
		return getInstance().readConfiguration(fullPathConfigurationFile);
	}

	/**
	 * Load configuration from <code>fullPathConfigurationFile</code>.
	 * 
	 * @param fullPathConfigurationFile
	 * @throws ConfigurationException
	 */
	private Configuration readConfiguration(String fullPathConfigurationFile)
			throws ConfigurationException {
		try {
			if (fullPathConfigurationFile != null) {

				File cfgFile = new File(fullPathConfigurationFile);
				if (cfgFile.exists()) {
					BufferedReader bufRead = new BufferedReader(new FileReader(
							cfgFile));
					String strLine = "";
					while ((strLine = bufRead.readLine()) != null) {
						;
						log.debug("read configuration line :\"" + strLine
								+ "\"");
						for (String groupName : groups.keySet()) {
							if (strLine.contains(groupName)) {
								Group<?> group = groups.get(groupName);
								log.info("parse configuration for "
										+ group.getGroupName() + "...");
								group.read(bufRead);
								log.info(group.getGroupName() + " done.");
								break;
							}
						}
					}
					// Close the input stream
					bufRead.close();

				} else {
					throw new ConfigurationException(String.format(
							"file [%s] does not exists !",
							fullPathConfigurationFile));
				}
			}

		} catch (MalformedURLException e) {
			log.fatal("Unable to read the configuration file", e);
			throw new ConfigurationException(
					String.format(
							"configuration filename \"configuration-file\" init-param for servlet Dispatecher ([%s]) path is malformed",
							fullPathConfigurationFile), e);
		} catch (FileNotFoundException e) {
			log.fatal(String.format("file [%s] does not exists !",
					fullPathConfigurationFile), e);
			throw new ConfigurationException(String.format(
					"file [%s] does not exists !", fullPathConfigurationFile),
					e);
		} catch (IOException e) {
			log.fatal(String.format(
					"Can not read the configuration file [%s] !",
					fullPathConfigurationFile), e);
			throw new ConfigurationException(String.format(
					"Can not read the configuration file [%s] !",
					fullPathConfigurationFile), e);
		}
		return this;
	}

	/**
	 * Retrieve the <code>group.key</code> parameter from the configuration
	 * file.
	 * 
	 * @param group
	 *            the <code>group</code> block from the configuration file
	 *            (delimited by a <code>[blockname]</code> notation)
	 * @param key
	 *            In this <code>group</code> block by an attribute
	 *            <code>key=value</code>.
	 * @return
	 */
	public static Item getParam(String pGroup, String pKey) {
		Item item = null;
		// for (String groupName : groups.keySet()) {
		if (groups.get(pGroup) != null) {
			Group<?> group = groups.get(pGroup);
			item = (Item) group.get(pKey);
			// break;
		}
		// }
		if (item == null) {
			log.info(String.format(
					"Parameter [%s.%s] not set. default value is used.",
					pGroup, pKey));
		}
		return item;
	}

	/**
	 * Return all items from the group <code>gName</code>.
	 * 
	 * @param gName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Item> getGroup(String gName) {
		return (Map<String, Item>) groups.get(gName).getAll();
	}

	/**
	 * return the corresponding <code>[group] / key</code> parameter.
	 * 
	 * @param group
	 * @param key
	 * @return
	 */
	public static Item get(String group, String key) {
		return getParam(group, key);
	}

	/**
	 * Verify in the configuration file is the <code>key</code> parameter is set
	 * to ON.
	 * 
	 * @param key
	 *            <code>key</code> String follow this format: "group.key".
	 * @return
	 */
	public static boolean isOn(String group, String key) {
		return ((Parameter) getParam(group, key)).isOn();
	}

	/**
	 * Verify in the configuration file is the <code>key</code> parameter is set
	 * to ON.
	 * 
	 * @param key
	 *            <code>key</code> String follow this format: "group:key".
	 * @return
	 */
	public static boolean isOn(String groupKey) {
		String[] groupKeyItem = groupKey.split(":");
		return ((Parameter) getParam(groupKeyItem[0], groupKeyItem[1])).isOn();
	}

	/**
	 * read all properties from group <code>groupName</code> from configuration
	 * file.
	 * 
	 * @param groupName
	 * @return
	 */
	public static Properties getPropertiesFromGroup(String groupName) {
		return groups.get(groupName).getAllProps();
	}

	/**
	 * Return and or instantiate the Singleton for Configuration.
	 * 
	 * @return
	 */
	public static Configuration getInstance() {

		if (instance == null) {
			instance = new Configuration();
		}
		return instance;
	}

}
