/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import fr.mcgivrer.framework.web.engine.configuration.modules.routes.RoutesGroup;
import fr.mcgivrer.framework.web.engine.services.authentication.Authentication;
import fr.mcgivrer.framework.web.engine.services.authentication.impl.Context;

/**
 * <p>
 * Service Manager
 * </p>
 * <p>
 * will parse code at services initialization to detect all @EasyService
 * implementation to instantiate and initialize each defined service.
 * </p>
 * <p>
 * According to EasyService <code>startlevel</code> attribute value, service
 * would be started in the right sort order.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class ServiceManager {

	/**
	 * internal logger.
	 */
	private static final Logger logger = Logger.getLogger(ServiceManager.class);

	private static final ServiceManager instance = new ServiceManager();

	/**
	 * list of service after initialization.
	 */
	private HashMap<String, Service> services = null;

	/**
	 * private constructor
	 */
	private ServiceManager() {
		init();
	}

	/**
	 * Initialize all detected services annotated with @EasyService.
	 */
	private void init() {
		services = new HashMap<String, Service>();

		ConfigurationBuilder servicesConfig = new ConfigurationBuilder()
				.addUrls(
						ClasspathHelper
								.forPackage(RoutesGroup.FRAMEWORK_PCK_BASE_CONTROLLER))
				.addUrls(ClasspathHelper.forClass(Service.class))
				.addUrls(ClasspathHelper.forClass(Authentication.class))
				.setScanners(new ResourcesScanner(),
						new TypeAnnotationsScanner(), new SubTypesScanner());

		Reflections servicesFilter = new Reflections(servicesConfig);
		for (Class<?> service : servicesFilter
				.getTypesAnnotatedWith(EasyService.class)) {
			// initialize service and add it to the services list.
			Object instanceService = null;
			try {
				EasyService easyServiceAnnotation = service
						.getAnnotation(EasyService.class);
				instanceService = service.newInstance();
				services.put(easyServiceAnnotation.name(),
						(Service) instanceService);
			} catch (InstantiationException e) {
				logger.fatal(
						"unable to instanciate the service ["
								+ service.getName() + "]", e);
			} catch (IllegalAccessException e) {
				logger.fatal(
						"unable to access the service [" + service.getName()
								+ "]", e);
			}
		}

	}

	/**
	 * Return list of available services as a Map with serviceName and service
	 * implementation.
	 * 
	 * @return
	 */
	private Map<String, Service> getServices() {
		return this.services;
	}

	/**
	 * retrieve the <code>serviceName</code> service and if exists return the
	 * already instantiated singleton.
	 * 
	 * @param serviceName
	 * @return
	 */
	public static synchronized Service get(String serviceName) {

		if (instance.getServices() != null
				&& instance.getServices().containsKey(serviceName)) {
			return instance.getServices().get(serviceName);
		} else {
			return null;
		}
	}

	/**
	 * retrieve the <code>serviceName</code> service and if exists return the
	 * already instantiated singleton.
	 * 
	 * @param serviceName
	 * @return
	 */
	public static synchronized Service get(String serviceName,Context context) {

		if (instance.getServices() != null
				&& instance.getServices().containsKey(serviceName)) {
			return instance.getServices().get(serviceName);
		} else {
			return null;
		}
	}

	
}
