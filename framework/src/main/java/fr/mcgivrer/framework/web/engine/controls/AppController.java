/**
 * 
 */
package fr.mcgivrer.framework.web.engine.controls;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.mcgivrer.framework.web.engine.services.i18n.Messages;
import fr.mcgivrer.framework.web.engine.services.render.PageRenderService;
import fr.mcgivrer.framework.web.engine.services.render.exceptions.PageRenderException;

/**
 * Generic Application Controller which must be extended by all Controllers.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class AppController implements Controller {
	/**
	 * Bindings for the rendering pipeline of the page.
	 */
	protected Map<String, Object> bindings = new HashMap<String, Object>();

    protected HttpServletRequest request = null;
    protected HttpServletResponse response = null;

    /*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.controls.Controller#bind(java.lang.String,
	 * java.lang.Object)
	 */
	public void bind(String key, Object value) {
		bindings.put(key, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.controls.Controller#bindMap(java.util.Map)
	 */
	public void bindMap(Map<String, String> values) {
		bindings.putAll(values);
	}

	/**
	 * remove an object from the page.
	 * 
	 * @param key
	 */
	public void remove(String key) {
		bindings.remove(key);
	}

	/**
	 * Remove all bindings.
	 */
	public void clearBinding() {
		bindings.clear();
	}

	/**
	 * return the <code>key</code> corresponding data from <code>bindings</code>
	 * .
	 * 
	 * @param key
	 *            key data to retrieve.
	 * @return value for this binding.
	 */
	public Object getBindingValue(String key) {
		return bindings.get(key);
	}

	/**
	 * @return the bindings
	 */
	public Map<String, Object> getBindings() {
		return bindings;
	}

	/**
	 * @param bindings
	 *            the bindings to set
	 */
	public void setBindings(Map<String, Object> bindings) {
		this.bindings = bindings;
	}

	/**
	 * Retrieve a i18n message from <code>messages*.properties</code> files.
	 * 
	 * @param messageKey
	 * @return
	 */
	public String getMessage(String messageKey) {
		return Messages.get(messageKey);
	}

	/**
	 * Return the composed message based on <code>messageKey</code> and
	 * parameterized with <code>args</code>.
	 * 
	 * @param messageKey
	 *            key of the message in the <code>properties</code> file.
	 * @param args
	 *            Parameters for the message (corresponding to the sorted %s tag
	 *            appearing in the message)
	 * @return
	 */
	public String getMessage(String messageKey, Object... args) {
		return Messages.get(messageKey, args);
	}

	/**
	 * Set default local to set a specific language.
	 * 
	 * @param locale
	 */
	public void setMessagesLocal(Locale locale) {
		Messages.setLocale(locale);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.controls.Controller#render(java.lang.String)
	 */
	public String render(String template) throws PageRenderException {
		return PageRenderService.getInstance().getRender().render(bindings, template);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.controls.Controller#render(java.lang.String,
	 * java.util.Map)
	 */
	public String render(String template, Map<String, Object> pBindings)
			throws PageRenderException {
		return PageRenderService.getInstance().getRender().render(pBindings, template);
	}

    /**
     * set the HttpServletRequest from Dispatcher.
     *
     * @param request
     */
    public void setRequest(HttpServletRequest request, HttpServletResponse resp) {
        this.request = request;
        this.response = resp;
    }

    /**
     * return the HttpServletRequest.
     *
     * @return
     */
    public HttpServletRequest getRequest() {
        return request;
    }

}
