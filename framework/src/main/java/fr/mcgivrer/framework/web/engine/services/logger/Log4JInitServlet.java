/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.BasicConfigurator;

/**
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class Log4JInitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	LoggerService loggerService = new LoggerService();

	private static String log4jLocation = "";

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		System.out.println("Log4JInitServlet is initializing log4j");
		log4jLocation = config.getInitParameter("log4j-config-file");

		ServletContext sc = config.getServletContext();
		String webAppPath = sc.getRealPath("/");
		if (log4jLocation == null) {
			System.err
					.println("*** No log4j-config-file init param, so initializing log4j with BasicConfigurator");
			BasicConfigurator.configure();
		} else {

			String configFilePath = webAppPath + log4jLocation;
			loggerService.readConfiguration(configFilePath);
		}
		super.init(config);
	}

}
