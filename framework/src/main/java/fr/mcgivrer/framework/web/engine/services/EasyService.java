/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * EasyService
 * </p>
 * <p>
 * The <code>EasyService</code> annotation is used to define a specific class as
 * a brand new Service.
 * </p>
 * <p>
 * the <code>startlevel</code> attribute will provide a start level to sort
 * services on the start list: lower value will start service first, upper value
 * will bring service at end of list, default is <code>100</code>.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EasyService {
	/**
	 * Name of the service.
	 */
	String name() default "service";

	/**
	 * Level ordering services start queue
	 * 
	 * @return
	 */
	int startlevel() default 100;
}
