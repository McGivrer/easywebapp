/**
 * 
 */
package fr.mcgivrer.framework.web.engine.configuration.modules.routes;

import java.io.Serializable;
import java.lang.reflect.Method;

import fr.mcgivrer.framework.web.engine.configuration.Item;
import fr.mcgivrer.framework.web.engine.controls.Controller;
import fr.mcgivrer.framework.web.engine.router.Dispatcher;

/**
 * @author FDELORME
 * 
 */
public class Route implements Item, Cloneable, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2992813997391723493L;

	/**
	 * Standard Http Methods.
	 * 
	 * @author FDELORME
	 * 
	 */
	public enum HttpMethod {
		PUT, GET, POST, DELETE, ALL
	}

	/**
	 * HTTP Type
	 */
	private HttpMethod httpMethod;
	/**
	 * Url pattern mapping.
	 */
	private String pattern = "";
	/**
	 * controller name and method.
	 */
	private String controllerMethodPath = "";
	/**
	 * Controller to instantiate.
	 */
	private Class<? extends Controller> controller;
	/**
	 * corresponding method in the controller to call.
	 */
	private Method method;

	/**
	 * Default constructor
	 */
	public Route() {
		super();
	}

	/**
	 * Default parameterized constructor. used by <code>Dispatcher</code> during
	 * <code>init</code> phase, reading configuration file.
	 * 
	 * @see Dispatcher#init()
	 * @param pattern
	 * @param controller
	 */
	public Route(String pattern, String controller) {
		this.controllerMethodPath = controller;
		this.pattern = pattern;
	}

	/**
	 * @param pattern
	 *            the pattern to set
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * @return the pattern
	 */
	public String getPattern() {
		return pattern;
	}

	/**
	 * @param controllerMethod
	 *            the controller method path to set
	 */
	public void setControllerMethodPath(String controllerMethod) {
		this.controllerMethodPath = controllerMethod;
	}

	/**
	 * @return the controller method path
	 */
	public String getControllerMethodPath() {
		return controllerMethodPath;
	}

	/**
	 * @param controller
	 *            the controller to set
	 */
	public void setController(Class<? extends Controller> controller) {
		this.controller = controller;
	}

	/**
	 * @return the controller
	 */
	public Class<? extends Controller> getController() {
		return controller;
	}

	/**
	 * @param method
	 *            the method to set
	 */
	public void setMethod(Method method) {
		this.method = method;
	}

	/**
	 * @return the method
	 */
	public Method getMethod() {
		return method;
	}

	/**
	 * @param mode
	 *            the mode to set
	 */
	public void setMode(HttpMethod mode) {
		this.httpMethod = mode;
	}

	/**
	 * @param mode
	 *            the mode to set
	 */
	public void setMode(String mode) {
		String modeLower = mode.toLowerCase();
		if (modeLower.equals("get")) {
			this.httpMethod = HttpMethod.GET;

		} else if (modeLower.equals("post")) {
			this.httpMethod = HttpMethod.POST;

		} else if (modeLower.equals("put")) {
			this.httpMethod = HttpMethod.PUT;

		} else if (modeLower.equals("delete")) {
			this.httpMethod = HttpMethod.DELETE;
		} else if (modeLower.equals("*")) {
			this.httpMethod = HttpMethod.ALL;
		}
	}

	/**
	 * @return the mode
	 */
	public HttpMethod getMode() {
		return httpMethod;
	}

	@Override
	public Route clone() {
		try {
			return (Route) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String toString() {
		return String.format("Route[patterm='%s',controller='%s',method='%s']",
				this.getPattern(), this.getController(), this.getMethod());
	}
}