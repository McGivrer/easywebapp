/**
 * 
 */
package fr.mcgivrer.framework.web.engine.configuration.modules.debug;

import fr.mcgivrer.framework.web.engine.configuration.ParameterGroup;

/**
 * @author fdelorme
 * 
 */
public class DebugGroup extends ParameterGroup {

	@Override
	public String getGroupName() {
		return "debug";
	}
}