/**
 * 
 */
package fr.mcgivrer.framework.web.engine.html.elements.input;

import fr.mcgivrer.framework.web.engine.html.HtmlPlugin;
import fr.mcgivrer.framework.web.engine.html.elements.FormElementHtmlPlugin;

/**
 * <p>
 * All HTML Input filed would inherit from this class.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public abstract class InputHtmlPlugin extends FormElementHtmlPlugin implements
		HtmlPlugin {

	/**
	 * return the type of Form Input element to render.
	 * 
	 * @return
	 */
	public abstract String getType();

	@Override
	public String getStartElement() {
		return "<input type=\"" + getType() + "\" %s />";
	}

	public String getEndElement() {
		return "";
	}

}
