/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.jobs;

import org.quartz.Trigger;

/**
 * @author mcgivrer
 * 
 */
public interface EasyJobGeneric{
	
	public void execute();

	public String event();

	public Trigger trigger();
	
	public String name();
}
