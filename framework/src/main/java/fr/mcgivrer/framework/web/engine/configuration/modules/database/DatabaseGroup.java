/**
 * 
 */
package fr.mcgivrer.framework.web.engine.configuration.modules.database;

import fr.mcgivrer.framework.web.engine.configuration.ParameterGroup;

/**
 * @author mcgivrer
 * 
 */
public class DatabaseGroup extends ParameterGroup {

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.mcgivrer.framework.web.engine.configuration.Group#getGroupName()
	 */
	@Override
	public String getGroupName() {
		return "database";
	}

}
