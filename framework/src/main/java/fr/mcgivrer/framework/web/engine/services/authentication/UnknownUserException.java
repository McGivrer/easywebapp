/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.authentication;

/**
 * @author FDELORME
 * 
 */
public class UnknownUserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5979387244298776483L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return super.getMessage();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#getLocalizedMessage()
	 */
	@Override
	public String getLocalizedMessage() {
		return super.getLocalizedMessage();
	}


	public UnknownUserException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnknownUserException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public UnknownUserException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public UnknownUserException(Throwable cause) {
		super(cause);
	}

}
