/**
 * 
 */
package fr.mcgivrer.framework.web.engine.html.elements.input;


/**
 * <p>
 * HTML Button basic implementation.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class ButtonHtmlPlugin extends InputHtmlPlugin{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.mcgivrer.framework.web.engine.html.elements.input.InputHtmlPlugin#
	 * getType()
	 */
	@Override
	public String getType() {
		return "button";
	}

	@Override
	public boolean getShowElementDecoration() {
		return false;
	}
	
	
}
