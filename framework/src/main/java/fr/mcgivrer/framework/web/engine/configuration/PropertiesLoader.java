/**
 * 
 */
package fr.mcgivrer.framework.web.engine.configuration;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import fr.mcgivrer.framework.web.engine.services.persistence.PersistenceService;

/**
 * Loading a properties file into a Properties object.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class PropertiesLoader {

	/**
	 * Internal PropertiesLoader Logger.
	 */
	private static final Logger logger = Logger
			.getLogger(PropertiesLoader.class);

	/**
	 * Load the properties fileName into a new properties object, and return it
	 * if loaded. return null if not.
	 * 
	 * @param fileName
	 *            name of properties file to load.
	 * @return
	 */
	public static Properties load(String fileName) {
		Properties props = null;
		if (fileName != null) {
			try {
				InputStream is = PersistenceService.class
						.getResourceAsStream("/"+fileName);
				if (is != null) {
					props = new Properties();
					props.load(is);
				}
			} catch (Exception e) {
				logger.fatal(String.format(
						"Configuration file '%s' not found.", fileName));
			}
		}
		return props;
	}
}
