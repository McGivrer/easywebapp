/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.persistence.exception;

/**
 * @author mcgivrer
 * 
 */
public class ORMException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7237150970362903900L;

	/**
	 * 
	 */
	public ORMException() {
	}

	/**
	 * @param message
	 */
	public ORMException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ORMException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ORMException(String message, Throwable cause) {
		super(message, cause);
	}

}
