/**
 * 
 */
package fr.mcgivrer.framework.web.engine.services.authentication;

/**
 * @author FDELORME
 * 
 */
public interface UserGeneric {
	public String getUserName();

	public String getFirstName();

	public String getLastName();

	public String getEmail();
	
	public String getPassword();
}
