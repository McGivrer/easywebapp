/*easywebapp.js*/
/**
 * Initialize the debug view panel (if needed).
 */
$('body').ready(
		function() {
			if($('#debug-view')){
				//Size Management.
				$('#debug-view a.button').click(function() {
					if ($('#debug-view').width() != "160") {

						$('#debug-view').width("160px").height("26px").css(
								"float", "right");

						$('#debug-view a.button').text("+");

						$('#debug-view pre').css("display", "none");

					} else {

						$('#debug-view').width("100%").height("100px").css(
								"float", "left");

						$('#debug-view a.button').text("-");

						$('#debug-view pre').css("display", "block");
					}
				});
			//Position management.	
			/*$(window).scroll(function() {
				$('#debug-view').scrollTop(0);
			});*/
		}
		$('.cover .media figure a').lightBox();
});
