/**
 *
 */
package fr.mcgivrer.framework.web.application.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import fr.mcgivrer.framework.web.engine.models.EasyEntity;

/**
 * A small Post entity to demonstrate this Small framework.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "findByCategory", query = "select p from Post p where p.category.id=:categoryId order by p.createdAt desc"),
		@NamedQuery(name = "findByAuthor", query = "select p from Post p where p.author.id=:userId order by p.createdAt desc"),
		@NamedQuery(name = "findByPlatform", query = "select p from Post p where p.plateform=:platformId order by p.createdAt desc"),
		@NamedQuery(name = "findByGametype", query = "select p from Post p where p.gameTypesList like :gameType order by p.createdAt desc")
})
public class Post extends EasyEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6111565478670962394L;

	/**
	 * Post title.
	 */
	@Length(min = 5, max = 100)
	@NotEmpty
	private String title;

	/**
	 * header text for this post.
	 */
	@Length(min = 0, max = 255)
	private String header;
	/**
	 * content text for this post.
	 */
	@Lob
	@NotEmpty
	private String content;
	/**
	 * Date of creation of this post.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	/**
	 * Author name for this post.
	 */
	@ManyToOne
	private User author;
	/**
	 * Category for this post.
	 */
	@ManyToOne
	private Category category;

	@Length(min = 2, max = 10)
	private String plateform;

	@Transient
	private String[] gameTypes;

	private String gameTypesList;

	/**
	 * Default constructor.
	 */
	public Post() {

	}

	/**
	 * All fields constructor.
	 * 
	 * @param title
	 * @param content
	 * @param author
	 * @param createdAt
	 */
	public Post(String title, String content, String header, User author,
			Category category, Date createdAt, String plateform,
			String[] gameTypes) {
		this.title = title;
		this.header = header;
		this.content = content;
		this.author = author;
		this.setCategory(category);
		this.createdAt = createdAt;
		this.plateform = plateform;
		setGameTypes(gameTypes);
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the author
	 */
	public User getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(User author) {
		this.author = author;
	}

	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @param header
	 *            the header to set
	 */
	public void setHeader(String header) {
		this.header = header;
	}

	@Override
	public boolean equals(Object obj) {
		Post post = ((Post) obj);
		return post.title.equals(title) && post.author.equals(author)
				&& post.createdAt.equals(createdAt);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	/**
	 * @return the plateform
	 */
	public String getPlateform() {
		return plateform;
	}

	/**
	 * @param plateform
	 *            the plateform to set
	 */
	public void setPlateform(String plateform) {
		this.plateform = plateform;
	}

	/**
	 * @return the gameTypes
	 */
	public String[] getGameTypes() {
		this.gameTypes = gameTypesList.split(",");
		return gameTypes;
	}

	/**
	 * @param gameTypes
	 *            the gameTypes to set
	 */
	public void setGameTypes(String[] gameTypes) {
		this.gameTypes = gameTypes;
		this.gameTypesList = "";
		for (String gt : gameTypes) {
			this.gameTypesList += (!this.gameTypesList.equals("") ? "," : "") + gt;
		}
	}

	public String getGameTypesList() {
		return gameTypesList;
	}

	public void setGameTypesList(String gameTypesList) {
		this.gameTypes = gameTypesList.split(",");
		this.gameTypesList = gameTypesList;
	}

	@Override
	public String toString() {
		return "Post[id:"
				+ id
				+ "title:'"
				+ title
				+ "',content:'"
				+ (content.length() > 25 ? content.substring(1, 25) + "..."
						: content) + "']";
	}
}
