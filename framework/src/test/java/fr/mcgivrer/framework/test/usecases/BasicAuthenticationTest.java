/**
 * 
 */
package fr.mcgivrer.framework.test.usecases;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.mcgivrer.framework.web.engine.services.ServiceManager;
import fr.mcgivrer.framework.web.engine.services.authentication.impl.BasicAuthentication;
import fr.mcgivrer.framework.web.engine.services.authentication.impl.BasicUser;

/**
 * @author mcgivrer
 * 
 */
public class BasicAuthenticationTest {
	BasicAuthentication ba = null;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		ba = (BasicAuthentication) ServiceManager
				.get("BasicAuthenticationService");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		ba = null;
	}


	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.services.authentication.impl.BasicAuthentication#getName()}
	 * .
	 */
	@Test
	public void testGetName() {
		assertEquals("BasicAnthentication not correclty named","BasicAuthentication",ba.getName());
		
	}



	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.services.authentication.impl.BasicAuthentication#authenticate(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testAuthenticate() {
		BasicUser user =  ba.authenticate("admin", "admin");
		assertEquals("user Administrator not exists !","administrator",user.getFirstName());
		
	}


	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.services.EasyServiceGeneric#stop()}
	 * .
	 */
	@Test
	public void testStop() {
		//fail("Not yet implemented");
	}

}
