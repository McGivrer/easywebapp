/**
 * 
 */
package fr.mcgivrer.framework.test.usecases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.mcgivrer.framework.web.application.entities.Category;
import fr.mcgivrer.framework.web.application.entities.Post;
import fr.mcgivrer.framework.web.application.entities.User;
import fr.mcgivrer.framework.web.engine.controls.AppController;
import fr.mcgivrer.framework.web.engine.services.i18n.Messages;
import fr.mcgivrer.framework.web.engine.services.render.exceptions.PageRenderException;

/**
 * Unit test for <code>AppController</code> class. {@link AppController}
 * 
 * @author mcgivrer
 * 
 */
public class AppControllerTest {

	private AppController appController = null;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		appController = new AppController();
		// set local to french for test purpose.
		Messages.setLocale(Locale.FRENCH);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		appController = null;
	}

	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.controls.AppController#bind(java.lang.String, java.lang.Object)}
	 * .
	 */
	@Test
	public void testBind() {
		appController.bind("key", new Integer(10));
		assertEquals("Bind does not add data to bindings hashmap !", true,
				appController.getBindingValue("key") != null);
	}

	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.controls.AppController#remove(java.lang.String)}
	 * .
	 */
	@Test
	public void testRemove() {
		appController.bind("key", new Integer(10));
		appController.remove("key");
		assertEquals("Bind does not remove data from bindings hashmap !", true,
				appController.getBindingValue("key") == null);
	}

	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.controls.AppController#clearBinding()}.
	 */
	@Test
	public void testClearBinding() {
		appController.bind("key", new Integer(10));
		appController.clearBinding();
		assertEquals("clear Bindings does not clear hashmap !", false,
				appController.getBindings() == null);
	}

	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.controls.AppController#getMessage(java.lang.String)}
	 * .
	 */
	@Test
	public void testGetMessageOnExistingOne() {
		// try to retrieve an existing message
		String appTitle = appController.getMessage("app.title");
		System.out.println("retrieve a message : appTitle='" + appTitle + "'");
		assertEquals(
				"Message 'app.title' does not retrieve from messages.properties !",
				true, appTitle.equals("test app"));

	}

	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.controls.AppController#getMessage(java.lang.String,Object[])}
	 * .
	 */
	@Test
	public void testGetMessageWithArgsOnExistingOne() {
		// try to retrieve an existing message
		String message = appController.getMessage("app.test.args",
				"composed with args");
		System.out.println("retrieve a parameterized message : app.test.args='"
				+ message + "'");
		assertEquals(
				"Message 'app.title' does not retrieve from messages.properties !",
				true, message.equals("This message is composed with args."));

	}

	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.controls.AppController#getMessage(java.lang.String)}
	 * .
	 */
	@Test
	public void testGetMessageOnNotExistingOne() {
		// try to retrieve an existing message
		String notExistingMessage = appController.getMessage("notexists");
		System.out
				.println("Not retrieve an unknown message : notExistingMessage='"
						+ notExistingMessage + "'");
		assertEquals(
				"Message 'notexists' must be identified as undefined key !",
				true, notExistingMessage.equals("key 'notexists' not defined"));
	}

	/**
	 * Simple test method for
	 * {@link fr.mcgivrer.framework.web.engine.controls.AppController#render(java.lang.String)}
	 * .
	 */
	@Test
	public void testRenderWithoutBinding() {
		String templateGenerated = null;
		try {
			appController.bind("title", "Games!");

			templateGenerated = appController.render("simple");
			assertEquals("Page generated is empty !", false,
					templateGenerated.equals(""));

			System.out.println(String.format("Generate simple page:[%s]",
					templateGenerated));

		} catch (PageRenderException e) {
			fail("Unable to read the template");
		}
	}

	/**
	 * More complex Test method for
	 * {@link fr.mcgivrer.framework.web.engine.controls.AppController#render(java.lang.String)}
	 * .
	 */
	@Test
	public void testRenderWithBinding() {
		String templateGenerated = null;
		Map<String, Object> bindings = new HashMap<String, Object>();
		try {

			bindings.put("pageTitle", "Games!");
			bindings.put("messages", Messages.getInstance());

			List<Post> posts = new ArrayList<Post>();
			posts.add(new Post("title1", "content1","",new User(),new Category(),new Date(),"ptf",new String[]{"gt1","gt2"}));
			posts.add(new Post("title2", "content1","",new User(),new Category(),new Date(),"ptf",new String[]{"gt1","gt2"}));
			posts.add(new Post("title3", "content1","",new User(),new Category(),new Date(),"ptf",new String[]{"gt1","gt2"}));
			bindings.put("posts", posts);

			templateGenerated = appController.render("complex", bindings);
			assertEquals("Page generated is empty !", false,
					templateGenerated.equals(""));

			System.out.println(String.format("Generate complexe page:\n[%s]",
					templateGenerated));

		} catch (PageRenderException e) {
			fail("Unable to read the template");
		}
	}

}
