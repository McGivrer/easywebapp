package fr.mcgivrer.framework.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.mcgivrer.framework.test.usecases.AppControllerTest;
import fr.mcgivrer.framework.test.usecases.BasicAuthenticationTest;
import fr.mcgivrer.framework.test.usecases.ConfigurationTest;
import fr.mcgivrer.framework.test.usecases.MessagesTest;

@RunWith(Suite.class)
@SuiteClasses({
	ConfigurationTest.class,
	MessagesTest.class,
	AppControllerTest.class,
	BasicAuthenticationTest.class})
public class AllTests {

}
