package fr.mcgivrer.framework.test.usecases;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.mcgivrer.framework.web.engine.configuration.Configuration;
import fr.mcgivrer.framework.web.engine.configuration.Item;
import fr.mcgivrer.framework.web.engine.configuration.Parameter;

/**
 * Unit test for <code>Configuration</code> class. {@link Configuration}
 * 
 * @author mcgivrer
 * 
 */
public class ConfigurationTest {
	Configuration config = null;

	@Before
	public void setUp() throws Exception {
		// Read the Unit test Configuration file.
		config = Configuration.getInstance().read(
				this.getClass().getResource("/").getPath()
						+ "configuration/test-application.ini");
	}

	@After
	public void tearDown() throws Exception {
		config = null;
	}

	@Test
	public void testGetParam() {
		Item valueParam = Configuration.getParam("debug", "showController");
		assertEquals("Can not retrieve the Item from configuration file", true,
				valueParam != null);
	}

	@Test
	public void testGet() {
		Item routeParam = Configuration.get("routes", "/index");
		assertEquals("Can not retrieve the Item from configuration file", true,
				routeParam != null);
	}

	@Test
	public void testIsOnStringString() {
		boolean valueParam = Configuration.isOn("debug", "testIsOn");
		assertEquals(
				"the On parameter from configuration file can not be retrieve to On.",
				true, valueParam);

		valueParam = Configuration.isOn("debug", "testIsOff");
		assertEquals(
				"the Off parameter from configuration file can not be retrieve to Off.",
				false, valueParam);
	}

	@Test
	public void testIsOnString() {
		boolean valueParam = Configuration.isOn("debug:testIsOn");
		assertEquals(
				"the On parameter from configuration file can not be retrieve to On.",
				true, valueParam);
		valueParam = Configuration.isOn("debug:testIsOff");
		assertEquals(
				"the Off parameter from configuration file can not be retrieve to Off.",
				false, valueParam);

	}

	@Test
	public void testGetRenderImplementation() {

		Configuration.getInstance();
		Parameter pageRenderImplClass = (Parameter) Configuration
				.get(Configuration.CFG_RENDER, "engine");
		assertEquals(
				"the 'render:engine' parameter from configuration file can not be retrieve to the right default value.",
				true,
				pageRenderImplClass
						.getValue()
						.equals("fr.mcgivrer.framework.web.engine.services.render.groovy.GroovyPageRender"));

	}

}
