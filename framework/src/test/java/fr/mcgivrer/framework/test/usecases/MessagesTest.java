/**
 * 
 */
package fr.mcgivrer.framework.test.usecases;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.mcgivrer.framework.web.engine.services.i18n.Messages;


/**
 * Unit Test for <code>Messages</code> class.
 * @author mcgivrer
 * @see Messages
 */
public class MessagesTest {

	Messages instance;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		// set default Locale to ENGLISH to perform first tests.
		Messages.setLocale(Locale.ENGLISH);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.services.i18n.Messages#getInstance()}.
	 */
	@Test
	public void testGetInstance() {
		Messages i18n = Messages.getInstance();
		assertEquals("Messages can not be initialized !", true, i18n != null);
	}
	
	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.services.i18n.Messages#get(java.lang.String)}
	 * .
	 */
	@Test
	public void testGetString() {
		String myTest = Messages.get("i18n.tests.test1");
		System.out.println("i18n.tests.test1=" + myTest);
		assertEquals("Message test1 can not be retrieve", true,
				myTest.equals("test1"));
	}

	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.services.i18n.Messages#get(java.lang.String, java.lang.Object[])}
	 * .
	 */
	@Test
	public void testGetStringObjectArray() {
		String myTest = Messages.get("i18n.tests.test2", "test2");
		System.out.println("i18n.tests.test2=" + myTest);
		assertEquals("Message test1 can not be retrieve", false,
				myTest.equals("test2:test2"));
	}

	/**
	 * Test method for
	 * {@link fr.mcgivrer.framework.web.engine.services.i18n.Messages#setLocale(java.util.Locale)}
	 * .
	 */
	@Test
	public void testSetLocale() {
		//FRENCH
		Messages.setLocale(Locale.FRENCH);
		String myTestfr = Messages.get("i18n.tests.locale");
		System.out.println("i18n.tests.locale=" + myTestfr);
		assertEquals("French locale was not loaded !", true,
				myTestfr.equals("Français"));
		// FRANCE
		Messages.setLocale(Locale.FRANCE);
		String myTestfrFR = Messages.get("i18n.tests.locale");
		System.out.println("i18n.tests.locale=" + myTestfrFR);
		assertEquals("French locale was not loaded !", true,
				myTestfrFR.equals("Française"));
	}


}
