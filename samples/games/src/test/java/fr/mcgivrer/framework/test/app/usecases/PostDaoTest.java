/**
 * 
 */
package fr.mcgivrer.framework.test.app.usecases;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.mcgivrer.framework.test.dao.DaoTestGeneric;
import fr.mcgivrer.framework.web.application.dao.CategoryDao;
import fr.mcgivrer.framework.web.application.dao.PostDao;
import fr.mcgivrer.framework.web.application.dao.UserDao;
import fr.mcgivrer.framework.web.application.entities.Category;
import fr.mcgivrer.framework.web.application.entities.Post;
import fr.mcgivrer.framework.web.application.entities.User;

/**
 * @author mcgivrer
 * 
 */
public class PostDaoTest extends DaoTestGeneric {

	private static PostDao postDao;
	private static CategoryDao categoryDao;
	private static UserDao userDao;

	private User user;
	private static Long postId;

	private static Category category1;

	@Before
	public void setup() {
		loadConfig();

		postDao = new PostDao();
		categoryDao = new CategoryDao();
		userDao = new UserDao();

		userDao.beginTransaction();
		user = new User("admin", "A", "Dministrator", "admin@application.com",
				"admin");
		userDao.save(user);
		userDao.commit();

		categoryDao.beginTransaction();
		category1 = new Category("Test1", "This is Test 1", "A small label 1",
				1);
		categoryDao.save(category1);
		categoryDao.commit();
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSave() {

		Post post = new Post("userdao-uc1", "userdao-uc1", "userdao-uc1", user,
				category1, new Date(), "ptf", new String[] { "gt1", "gt2" });

		postDao.beginTransaction();
		post = postDao.save(post);
		postDao.commit();
		postId = post.getId();

		assertEquals("save() does not retrieve Post", false,
				post.getId() == null);
	}

	@Test
	public void testFindById() {
		Post post = postDao.findById(postId);
		System.out.println("post=" + post.toString());
		assertEquals("findById() does not retrieve Post", false, post == null);
	}

	@Test
	public void testCount() {
		int postCount = postDao.countAll();
		assertEquals("number of post is 0 !", true, postCount != 0);

	}

	@Test
	public void testFindByCategory() {
		Post post = new Post("userdao-uc1", "userdao-uc1", "userdao-uc1", user,
				category1, new Date(), "ptf", new String[] { "gt1", "gt2" });

		postDao.beginTransaction();
		post = postDao.save(post);
		postDao.commit();
		postId = post.getId();

		List<Post> posts = postDao.findByCategory(category1.getId());
		assertEquals("Can not retrieve post on category id!", true,
				posts.size() == 1);
	}

	@Test
	public void testDelete() {
		Post post = postDao.findById(postId);
		if (post != null) {
			postDao.delete(post);
			post = postDao.findById(postId);
			assertEquals("delete() does not delete Post", true, post == null);
		}

	}

}
