/**
 * 
 */
package fr.mcgivrer.framework.test.app.usecases;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.mcgivrer.framework.test.dao.DaoTestGeneric;
import fr.mcgivrer.framework.web.application.dao.CategoryDao;
import fr.mcgivrer.framework.web.application.entities.Category;

/**
 * @author mcgivrer
 * 
 */
public class CategoryDaoTest extends DaoTestGeneric {

	private static CategoryDao categoryDao;

	private static Long categoryId;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		loadConfig();
		categoryDao = new CategoryDao();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSave() {
		Category category = new Category("categorytest-title",
				"categorytest-label", "categorytest-description",1);
		categoryDao.beginTransaction();
		categoryDao.save(category);
		categoryDao.commit();
		assertEquals("Unable to save category to database.", false,
				category.getId() == null);

		categoryId = category.getId();

	}

	@Test
	public void testFindById() {
		Category category = categoryDao.findById(categoryId);
		assertEquals("Unable to read Category[id:" + categoryId
				+ "] from database.", false, category == null);
	}

	@Test
	public void testDelete() {
		Category category = categoryDao.findById(categoryId);
		if (category != null) {
			categoryDao.beginTransaction();
			categoryDao.delete(category);
			categoryDao.commit();
			category = categoryDao.findById(categoryId);
			assertEquals("CategoryDao#delete() does not delete Category", true,
					category == null);
		}
	}

}
