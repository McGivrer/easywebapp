package fr.mcgivrer.framework.test.app;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.mcgivrer.framework.test.app.usecases.CategoryDaoTest;
import fr.mcgivrer.framework.test.app.usecases.PostDaoTest;
import fr.mcgivrer.framework.test.app.usecases.UserDaoTest;

@RunWith(Suite.class)
@SuiteClasses({
	UserDaoTest.class,
	CategoryDaoTest.class,
	PostDaoTest.class})
public class AllTests {

}
