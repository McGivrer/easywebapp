/**
 * 
 */
package fr.mcgivrer.framework.test.app.usecases;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.mcgivrer.framework.test.dao.DaoTestGeneric;
import fr.mcgivrer.framework.web.application.dao.UserDao;
import fr.mcgivrer.framework.web.application.entities.User;

/**
 * @author mcgivrer
 * 
 */
public class UserDaoTest extends DaoTestGeneric {

	private static UserDao userDao;

	private static Long userId;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		loadConfig();
		userDao = new UserDao();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSave() {
		User user = new User("usertest", "usertest", "userstest",
				"usertest@test.com", "usertest");
		userDao.beginTransaction();
		userDao.save(user);
		userDao.commit();
		assertEquals("Unable to save user to database.", false,
				user.getId() == null);

		userId = user.getId();

	}

	@Test
	public void testFindById() {
		User user = userDao.findById(userId);
		assertEquals("Unable to read User[id:" + userId + "] from database.",
				false, user == null);
	}

}
