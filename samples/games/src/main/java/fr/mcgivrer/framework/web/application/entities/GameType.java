/**
 * 
 */
package fr.mcgivrer.framework.web.application.entities;

/**
 * @author FDELORME
 *
 */
public class GameType extends Couple {

	public GameType(String id, String title) {
		super(id, title);
	}

}
