/**
 * 
 */
package fr.mcgivrer.framework.web.application.jobs;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Trigger;

import de.svenjacobs.loremipsum.LoremIpsum;
import fr.mcgivrer.framework.web.application.dao.CategoryDao;
import fr.mcgivrer.framework.web.application.dao.PostDao;
import fr.mcgivrer.framework.web.application.dao.UserDao;
import fr.mcgivrer.framework.web.application.entities.Category;
import fr.mcgivrer.framework.web.application.entities.Post;
import fr.mcgivrer.framework.web.application.entities.User;
import fr.mcgivrer.framework.web.engine.job.EasyJob;
import fr.mcgivrer.framework.web.engine.services.jobs.EasyJobGeneric;
import fr.mcgivrer.framework.web.engine.services.jobs.JobManager;

/**
 * A sample "Application Start" Job demonstrating the <code>@EasyJob</code>
 * annotation usage.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
@EasyJob(name = "Application Initialization", event = JobManager.EVENTS.ONSTART)
public class OnApplicationFirstStart implements EasyJobGeneric {

	private static final Logger logger = Logger
			.getLogger(OnApplicationFirstStart.class);

	/**
	 * DAO to retrieve data.
	 */
	private PostDao postDao = new PostDao();
	private CategoryDao categoryDao = new CategoryDao();
	private UserDao userDao = new UserDao();

	/**
	 * <p>
	 * A small setup on application startup.
	 * </p>
	 * <p>
	 * TODO see solution proposed by Playframework with
	 * <code>@OnApplicationStart</code> annotation.
	 * </p>
	 */
	public void execute(){
		try {
			if (userDao.countAll() == 0) {
				userDao.beginTransaction();
				userDao.save(new User("admin", "A", "Dministrator",
						"admin@application.com", "admin"));
				userDao.commit();
			}
			Category cat1 = null;
			Category cat2 = null;
			if (categoryDao.countAll() == 0) {
				categoryDao.beginTransaction();
				cat1 = new Category("Test-1", "This is Test 1",
						"A small label 1", 1);
				cat2 = new Category("Test-2", "This is Test 2",
						"A small label 2", 2);
				categoryDao.save(cat1);
				categoryDao.save(cat2);
				categoryDao.commit();
			}

			cat1 = categoryDao.findByName("Test-1");
			cat2 = categoryDao.findByName("Test-2");

			User admin = userDao.findById(new Long(1));
			
			LoremIpsum loremIpsum = new LoremIpsum();
			if (postDao.countAll() == 0) {
				postDao.beginTransaction();
				Post post = null;
				for (int i = 1; i < 7; i++) {
					post = new Post(loremIpsum.getWords(4), "<p>"
							+ loremIpsum.getParagraphs(5).replaceAll("\\n",
									"</p><p>") + "</p>", "<p>"
							+ loremIpsum.getWords(20) + "</p>", admin,
							(i > 3 ? cat1 : cat2), new Date(), "x360", new String[]{"Action","Adventure"});
					postDao.save(post);
				}
				postDao.commit();
			}


		} catch (Exception he) {
			logger.error("Unable to commit data", he);
		}
	}

	@Override
	public String event() {
		return "ONSTART";
	}

	@Override
	public Trigger trigger() {
		return null;
	}

	@Override
	public String name() {
		return this.getClass().getSimpleName();
	}

}
