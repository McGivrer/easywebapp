/**
 * 
 */
package fr.mcgivrer.framework.web.application.dao;

import javax.persistence.TypedQuery;

import fr.mcgivrer.framework.web.application.entities.Category;
import fr.mcgivrer.framework.web.engine.dao.DaoGeneric;

/**
 * <p>
 * Category Manager
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class CategoryDao extends DaoGeneric<Category, Long> {

	/**
	 * Retrieve a specific category on its <code>name</code>.
	 * 
	 * @param name
	 * @return
	 */
	public Category findByName(String name) {
		TypedQuery<Category> query = entityManager.createNamedQuery(
				"findByName", Category.class);
		query.setParameter("title", name);
		Category category = query.getResultList().get(0);
		return category;
	}

}
