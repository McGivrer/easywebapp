/**
 * 
 */
package fr.mcgivrer.framework.web.application.entities;

/**
 * Game Platform entity.
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
public class Couple {
	/**
	 * Platform identity.
	 */
	private String id;

	/**
	 * Platform title.
	 */
	private String title;

	/**
	 * @param id
	 * @param title
	 */
	public Couple(String id, String title) {
		super();
		this.id = id;
		this.title = title;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}
