/**
 * 
 */
package fr.mcgivrer.framework.web.application.dao;

import fr.mcgivrer.framework.web.application.entities.User;
import fr.mcgivrer.framework.web.engine.dao.DaoGeneric;

/**
 * @author mcgivrer
 *
 */
public class UserDao extends DaoGeneric<User, Long> {

}
