/**
 * 
 */
package fr.mcgivrer.framework.web.application.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.persistence.TypedQuery;

import fr.mcgivrer.framework.web.application.entities.Post;
import fr.mcgivrer.framework.web.engine.dao.DaoGeneric;

/**
 * <p>
 * Manage Post entity
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 * 
 */
@Resource
public class PostDao extends DaoGeneric<Post, Long> {

	/**
	 * retrieve all posts for the selected <code>categoryId</code>.
	 * 
	 * @param categoryId
	 *            Long Id od the Category to be filtered on.
	 * @return
	 */
	public List<Post> findByCategory(Long categoryId) {
		TypedQuery<Post> query = entityManager.createNamedQuery(
				"findByCategory", Post.class);
		query.setParameter("categoryId", categoryId);
		List<Post> posts = query.getResultList();
		return posts;
	}

	/**
	 * retrieve all posts for the selected <code>userId</code>.
	 * 
	 * @param userId
	 *            Long Id of the User to be filtered on.
	 * @return
	 */
	public List<Post> findByAuthor(String userId) {
		TypedQuery<Post> query = entityManager.createNamedQuery("findByAuthor",
				Post.class);
		query.setParameter("userId", userId);
		List<Post> posts = query.getResultList();
		return posts;
	}

	/**
	 * retrieve all posts for the selected <code>platformId</code>.
	 * 
	 * @param userId
	 *            Long Id of the User to be filtered on.
	 * @return
	 */
	public List<Post> findByPlatform(String platformId) {
		TypedQuery<Post> query = entityManager.createNamedQuery("findByPlatform",
				Post.class);
		query.setParameter("platformId", platformId);
		List<Post> posts = query.getResultList();
		return posts;
	}
	
	/**
	 * retrieve all posts where <code>gameType</code> is part of the gamesTypeList.
	 * 
	 * @param userId
	 *            Long Id of the User to be filtered on.
	 * @return
	 */
	public List<Post> findByGameType(String gameType) {
		TypedQuery<Post> query = entityManager.createNamedQuery("findByGametype",
				Post.class);
		query.setParameter("gameType", gameType);
		List<Post> posts = query.getResultList();
		return posts;
	}
}
