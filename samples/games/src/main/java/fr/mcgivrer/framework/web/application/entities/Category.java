/**
 * 
 */
package fr.mcgivrer.framework.web.application.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

import fr.mcgivrer.framework.web.engine.models.EasyEntity;

/**
 * @author mcgivrer
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "findByName", query = "select c from Category c where c.title=:title order by c.sortOrder asc") })
public class Category extends EasyEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5398880285193131789L;

	/**
	 * title for this category.
	 */
	@Column(nullable = false)
	@Size(min = 4, max = 20)
	private String title;

	@Size(min = 0, max = 60)
	private String label;

	@Size(min = 0, max = 255)
	private String description;

	private int sortOrder;

	@OneToMany
	private List<Post> posts;

	/**
	 * Category default constructor.
	 */
	public Category() {

	}

	/**
	 * @param title
	 * @param label
	 * @param description
	 */
	public Category(String title, String label, String description,int sortOrder) {
		super();
		this.title = title;
		this.label = label;
		this.description = description;
		this.sortOrder=sortOrder;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the posts
	 */
	public List<Post> getPosts() {
		return posts;
	}

	/**
	 * @param posts
	 *            the posts to set
	 */
	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	/**
	 * 
	 * @return
	 */
	public int getSortOrder() {
		return sortOrder;
	}

	/**
	 * 
	 * @param sortOrder
	 */
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

}
