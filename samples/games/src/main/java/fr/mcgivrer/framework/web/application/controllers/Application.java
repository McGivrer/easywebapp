/**
 *
 */
package fr.mcgivrer.framework.web.application.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import fr.mcgivrer.framework.web.application.dao.CategoryDao;
import fr.mcgivrer.framework.web.application.dao.PostDao;
import fr.mcgivrer.framework.web.application.dao.UserDao;
import fr.mcgivrer.framework.web.application.entities.Category;
import fr.mcgivrer.framework.web.application.entities.GameType;
import fr.mcgivrer.framework.web.application.entities.Platform;
import fr.mcgivrer.framework.web.application.entities.Post;
import fr.mcgivrer.framework.web.application.entities.User;
import fr.mcgivrer.framework.web.engine.controls.AppController;
import fr.mcgivrer.framework.web.engine.controls.Controller;
import fr.mcgivrer.framework.web.engine.controls.annotations.AfterControl;
import fr.mcgivrer.framework.web.engine.controls.annotations.BeforeControl;
import fr.mcgivrer.framework.web.engine.html.form.FormInput;
import fr.mcgivrer.framework.web.engine.html.form.attributes.AttributeHelper;
import fr.mcgivrer.framework.web.engine.services.ServiceManager;
import fr.mcgivrer.framework.web.engine.services.authentication.UnknownUserException;
import fr.mcgivrer.framework.web.engine.services.authentication.impl.BasicAuthentication;

/**
 * <p>
 * Default start class for this sample application.
 * </p>
 * 
 * @author Frédéric Delorme<frederic.delorme@gmail.com>
 */
public class Application extends AppController implements Controller {

	private Logger logger = Logger.getLogger(Application.class);

	/**
	 * DAO to retrieve data.
	 */
	private PostDao postDao = new PostDao();
	private CategoryDao categoryDao = new CategoryDao();
	private UserDao userDao = new UserDao();

	/**
	 * List of game platforms.
	 */
	private List<Platform> platforms = new ArrayList<Platform>();

	/**
	 * List of game types.
	 */
	private List<GameType> gameTypes = new ArrayList<GameType>();

	/**
	 * Connected user (or null).
	 */
	private User user = null;

	/**
	 * Filter criteria Category.
	 */
	private Long categoryId;

	/**
	 * Filter criteria Platform.
	 */
	private String platformId;

	/**
	 * Filter criteria game type.
	 */
	private String gameTypeId;

	/**
	 * Basic user authentication
	 */
	private BasicAuthentication ba = null;

	@BeforeControl
	public void setup() {
		ba = (BasicAuthentication)ServiceManager.get("BasicAuthenticationService");

        initializePlatformsList();
		initializeGameTypesList();

		categoryId = (Long) Long.parseLong((String) AttributeHelper.get(
				request, "categoryId", "0"));

		platformId = (String) AttributeHelper.get(request, "platformId", "");

		gameTypeId = (String) AttributeHelper.get(request, "gameTypeId", "");

		bind("platformId", platformId);

		bind("gameTypeId", "%" + gameTypeId + "%");

		bind("categoryId", categoryId);

		bind("title", "Games !");

		// prepare list of categories
		List<Category> categories = categoryDao.findAll();
		bind("categories", categories);

	}

	/**
	 * 
	 */
	private void initializePlatformsList() {
		platforms.add(new Platform("x360", "X360"));
		platforms.add(new Platform("ps3", "PS3"));
		platforms.add(new Platform("pc", "PC"));
		platforms.add(new Platform("nds", "NDS"));

		bind("platforms", platforms);
	}

	private void initializeGameTypesList() {
		gameTypes.add(new GameType("adventure", "Adventure"));
		gameTypes.add(new GameType("action", "Action"));
		gameTypes.add(new GameType("rpg", "RPG"));
		gameTypes.add(new GameType("fps", "FPS"));
		gameTypes.add(new GameType("rts", "RTS"));
		gameTypes.add(new GameType("gtalike", "GTA-Like"));

		bind("gameTypes", gameTypes);
	}

	/**
	 * A handle to perform.
	 */
	@AfterControl
	public void destroy() {
	}

	/**
	 * Display main page.
	 * 
	 * @return
	 */
	public String index() {
		bind("categoryId", new Long(0));

		request.getSession().removeAttribute("categoryId");

		try {
		
			user = (User) ba.getUser(request);
			bind("user",user);

		} catch (UnknownUserException e) {
			
			logger.debug("Unknwon user : user is not connected !", e);
		
		}
		List<Post> posts = postDao.findAll();
		bind("posts", posts);

		return "application/index";
	}

	/**
	 * Show a specific post.
	 * 
	 * @return
	 */
	public String show() {

		String postId = request.getParameter("postId");
		Post post = postDao.findById(Long.parseLong(postId));
		bind("post", post);

		List<Post> posts;
		if (categoryId != null) {
			posts = postDao.findByCategory(categoryId);
		} else {
			posts = postDao.findAll();
		}
		bind("posts", posts);

		return "application/show";
	}

	/**
	 * Display posts filter by category.
	 * 
	 * @return
	 */
	public String category() {

		List<Post> posts = postDao.findByCategory(categoryId);
		bind("posts", posts);

		return "application/index";
	}

	/**
	 * Display posts filter by type of game.
	 * 
	 * @return
	 */
	public String type() {

		List<Post> posts = postDao.findByGameType(gameTypeId);
		bind("posts", posts);

		return "application/index";
	}

	/**
	 * Display posts filter by category.
	 * 
	 * @return
	 */
	public String platform() {

		List<Post> posts = postDao.findByPlatform(platformId);
		bind("posts", posts);

		List<Category> categories = categoryDao.findAll();
		bind("categories", categories);

		return "application/index";
	}

	/**
	 * Create a new post.
	 * 
	 * @return
	 */
	public String create() {
		List<Category> categories = categoryDao.findAll();
		bind("categories", categories);

		Post post = new Post("My First Form", "This is really my first form",
				"This is the header", userDao.findById(Long.parseLong("1")),
				categoryDao.findById(Long.parseLong("1")), new Date(), "x360",
				new String[] { "Adventure", "Action" });
		bind("post", post);
		return "application/create";
	}

	/**
	 * Save the just created Post.
	 * 
	 * @return
	 */
	public String save() {
		Post post = (Post) FormInput.getEntity(request, "post", Post.class);
		bind("post", post);
		return "application/create";
	}
}
