/**
 * 
 */
package fr.mcgivrer.framework.web.application.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import fr.mcgivrer.framework.web.engine.services.authentication.impl.BasicUser;

/**
 * User entity for this sample project.
 * 
 * @author Frédéric Delorme<fredeic.delorme@gmail.com>
 * 
 */
@Entity
public class User extends BasicUser {

	/**
	 * posts authored by the user.
	 */
	@OneToMany
	private List<Post> posts;

	/**
	 * 
	 */
	public User() {
		super();
	}

	/**
	 * @param userName
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param password
	 */
	public User(String userName, String firstName, String lastName,
			String email, String password) {
		super(userName, firstName, lastName, email, password);
	}

	/**
	 * @param userAttrs
	 */
	public User(String[] userAttrs) {
		super(userAttrs);
	}

	/**
	 * @return the posts
	 */
	public List<Post> getPosts() {
		return posts;
	}

	/**
	 * @param posts
	 *            the posts to set
	 */
	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1509985380791894917L;

}